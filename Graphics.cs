﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Programm
{
    public partial class Graphics : Form
    {
        SqlConnection dataBaseConnection;
        int idGraphic = -1;     //определение графика для построения
        public Graphics(SqlConnection sql, int flag)
        {
            dataBaseConnection = sql;
            idGraphic = flag;
            InitializeComponent();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Graphics_Load(object sender, EventArgs e)
        {
            DataSet ds;
            ControlBox = false;
            switch (idGraphic)
            {
                case 1:     //Абонплата
                    DataShow.Titles.Clear();
                    DataShow.Titles.Add("Диаграмма тарифов на основе абонентских плат");
                    //DataShow.DataSource = 
                    DataShow.Update();
                    break;
                case 2:     //Среднее пополнение
                    DataShow.Titles.Clear();
                    DataShow.Titles.Add("Диаграмма среднего пополнения номеров");
                    break;
                case 3:     //Пополнения
                    DataShow.Titles.Clear();
                    DataShow.Titles.Add("Диаграмма суммарного пополнения счета абонентами"); //(сделать sum(сумма) груп бай номер и залить сюда
                    break;
            }
        }
    }
}
