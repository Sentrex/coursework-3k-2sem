﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using SD = System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Excel = Microsoft.Office.Interop.Excel;

namespace Programm
{
    public partial class Log : Form
    {
        SqlConnection dataBaseConnection;
        public Log(SqlConnection cnct)
        {
            dataBaseConnection = cnct;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Log_Load(object sender, EventArgs e)
        {
            dateTimePicker1.Value = DateTime.Parse("01-01-2001");
            RefreshData();
            ControlBox = false;
        }

        private void RefreshData()
        {
            String date1 = dateTimePicker1.Value.ToString("dd-MM-yyyy");
            String date2 = dateTimePicker2.Value.ToString("dd-MM-yyyy");
            String info = textBox1.Text;
            SqlDataAdapter da = new SqlDataAdapter("Select username as 'Логин пользователя', info as 'Проведенная операция', время as 'Время', дата as 'Дата проведения операции', код from LogChanges where info like '" + info + "%' and дата between '" + date1 + "' and '" + date2 + "' order by дата desc, время desc", dataBaseConnection);
            SD.DataSet ds = new SD.DataSet();
            da.Fill(ds, "Лог");
            dataGridView1.DataSource = ds.Tables["Лог"];
            dataGridView1.Columns[4].Visible = false;
            dataGridView1.Update();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        //Отчет
        private void button2_Click(object sender, EventArgs e)
        {
            string path = null;
            using (var dialog = new FolderBrowserDialog())
                if (dialog.ShowDialog() == DialogResult.OK)
                    path = dialog.SelectedPath;
            Excel.Application exApp = new Excel.Application();      //создание объекта
            exApp.Visible = false;
            exApp.Workbooks.Add();  //новая рабочая книга
            Excel.Worksheet workSheet = (Excel.Worksheet)exApp.ActiveSheet;         //получили активный лист
            workSheet.Cells[1, 1] = "Логин пользователя";
            workSheet.Cells[1, "B"] = "Проведенная операция";
            workSheet.Cells[1, "C"] = "Время";
            workSheet.Cells[1, "D"] = "Дата";
            int rowExcel = 2; //начать со второй строки.
            
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                //заполняем строку
                workSheet.Cells[rowExcel, "A"] = dataGridView1.Rows[i].Cells["Логин пользователя"].Value;
                workSheet.Cells[rowExcel, "B"] = dataGridView1.Rows[i].Cells["Проведенная операция"].Value;
                workSheet.Cells[rowExcel, "C"] = dataGridView1.Rows[i].Cells["Время"].Value.ToString();
                workSheet.Cells[rowExcel, "D"] = dataGridView1.Rows[i].Cells["Дата проведения операции"].Value;

                rowExcel++;
            }
            workSheet.get_Range("A1", "D1").EntireColumn.AutoFit();
            String pathToXmlFile;
            pathToXmlFile = path + "Report.xlsx";
            workSheet.SaveAs(pathToXmlFile);
            exApp.Quit();
            MessageBox.Show("Экспорт активного лога успешно выполнен.", "Успешное выполнение операции", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
