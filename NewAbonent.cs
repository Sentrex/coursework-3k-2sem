﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Programm
{
    public partial class NewAbonent : Form
    {
        SqlConnection dataBaseConnection;
        DataSet ds;
        SqlDataAdapter clientsAdapter;  //для клиентов
        SqlDataAdapter emplAdapter;     //для сотрудников
        SqlDataAdapter tarAdapter;      //для тарифов
        String login;
        public NewAbonent(SqlConnection sql, String lg)
        {
            login = lg;
            dataBaseConnection = sql;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NewAbonent_Load(object sender, EventArgs e)
        {
            RefreshData();
            ControlBox = false;
        }

        private void RefreshData()
        {
            ds = new DataSet();
            String search1 = textBox1.Text;
            String search2 = textBox2.Text;
            emplAdapter = new SqlDataAdapter("Select ФИО, Должность, код as 'Код сотрудника' from Сотрудники where ФИО like '" + search1 +"%' order by ФИО", dataBaseConnection);       //получили сотрудников
            emplAdapter.Fill(ds, "Сотрудники");
            clientsAdapter = new SqlDataAdapter("SELECT ФИО, дата_рождения as 'Дата рождения', дата_регистрации as 'Дата регистрации', код 'Код клиента' FROM Клиенты where ФИО like '" + search2 + "%' order by ФИО", dataBaseConnection);       //получили клиентов
            clientsAdapter.Fill(ds, "Клиенты");
            Employees.DataSource = ds.Tables["Сотрудники"]; //отображение сотрудников
            Clients.DataSource = ds.Tables["Клиенты"]; //отображение клиентов
            Clients.Columns[3].Visible = false;
            Employees.Columns[2].Visible = false;
            Employees.Update();
            Clients.Update();
            tarAdapter = new SqlDataAdapter("select название, код from Тарифы order by название", dataBaseConnection);
            tarAdapter.Fill(ds, "Тарифы");
            Tariffs.DataSource = ds.Tables["Тарифы"];
            Tariffs.Columns[1].Visible = false;
            Tariffs.Update();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            String Tarif = (String)Tariffs.SelectedCells[0].Value;  //получили выбранный тариф
            int ClientCode = (int)Clients["Код клиента", Clients.CurrentCell.RowIndex].Value;   //получили код выбранного клиента
            String ClientName = (String)Clients["ФИО", Clients.CurrentCell.RowIndex].Value;     //ФИО для вывода
            String EmployeeName = (String)Employees["ФИО", Employees.CurrentCell.RowIndex].Value;                     //ФИО для вывода
            int EmployeeCode = (int)Employees["Код сотрудника", Employees.CurrentCell.RowIndex].Value;   //получение кода для запроса
            int TariffCode = (int)Tariffs["код", Tariffs.CurrentCell.RowIndex].Value;           //код тарифа
            if (Number.MaskFull)
            {
                if (Code.Items.Contains(Code.Text))
                {
                    String number = Code.SelectedItem + Number.Text.Replace("-", "");           //получили и отформатировали номер
                    String Message = "Подтвердите введенные данные:\n\n Номер:                      " + Code.SelectedItem + " " + Number.Text;
                    Message += "\nФИО владельца:       " + ClientName + "\nФИО регистратора:  " + EmployeeName + "\nТариф:                        " + Tarif + "";
                    if (MessageBox.Show(Message, "Подтверждение введенных данных.", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        DateTime nowDate = DateTime.Today;                  //сегодняшняя дата
                        String TimeReg = nowDate.ToString("yyyy-MM-dd");    //конвертирование под формат сервера
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = dataBaseConnection;
                        cmd.CommandText = "Insert into Номера values ('" + number + "', " + EmployeeCode + ", " + ClientCode + ", " + TariffCode + ", '" + TimeReg + "', NULL)";    //запись в базу
                        dataBaseConnection.Open();
                        try { cmd.ExecuteNonQuery(); }      //выполнение запроса
                        catch (SqlException)
                        {
                            MessageBox.Show("Такой номер уже существует. Введите другой.", "Ошибка регистрации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Number.Clear();
                            dataBaseConnection.Close();
                            return;
                        }
                        dataBaseConnection.Close();
                        DateTime time = DateTime.Today;
                        SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + login + "', 'Зарегистрировал номер " + number + "', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                        dataBaseConnection.Open();
                        cmd1.ExecuteNonQuery();
                        dataBaseConnection.Close();
                        if (MessageBox.Show("Регистрация успешно завершена.\n\n\n\nЖелаете продолжить регистрацию?", "Успешное выполнение операции", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            RefreshData();  //обновление данных
                            Number.Clear(); //очистка данных
                            Code.Text = ""; //то же самое
                            RegistrationClient.Visible = false;
                        }
                        else
                        {
                            this.Close();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Некорректный код номера!", "Ошибка регистрации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Code.Text = "";
                }
            }
            else
            {
                MessageBox.Show("Введен некорректный номер. Повторите ввод.", "Ошибка ввода", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Number.Focus();
                Number.Clear();
            }
        }

        private void regClient_Click(object sender, EventArgs e)
        {
            RegistrationClient.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String fio = FIO.Text;      //получение фио клиента
            String Date = dateTimePicker1.Value.ToString("yyyy-MM-dd"); 
            DateTime nowDate = DateTime.Today;                  //сегодняшняя дата
            String TimeReg = nowDate.ToString("yyyy-MM-dd");    //конвертирование под формат сервера
            SqlCommand cmd = new SqlCommand("Insert into Клиенты values('" + fio + "', '" + Date + "', '" + TimeReg + "')");
            cmd.Connection = dataBaseConnection;
            dataBaseConnection.Open();
            cmd.ExecuteNonQuery();
            dataBaseConnection.Close();
            DateTime time = DateTime.Today;
            SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + login + "', 'Зарегистрировал нового клиента " + fio + "', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
            dataBaseConnection.Open();
            cmd1.ExecuteNonQuery();
            dataBaseConnection.Close();
            RegistrationClient.Visible = false;
            FIO.Clear();
            RefreshData();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            RegistrationClient.Visible = false;
            FIO.Clear();
        }
        

    }
}
