﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Programm
{
    public partial class Procedures : Form
    {
        SqlConnection dataBaseConnection;
        public Procedures(SqlConnection sql)
        {
            InitializeComponent();
            dataBaseConnection = sql;
        }

        private void Procedures_Load(object sender, EventArgs e)
        {
            this.toolTip1.SetToolTip(radioButton1, "Список номеров во владении заданного клиента и их дата подключения");
            this.toolTip2.SetToolTip(radioButton2, "Список предоставленных услуг для данного номера и указанного названия типа услуги");
            this.toolTip3.SetToolTip(radioButton3, "Количество членов в наиболее длинной подпоследовательности из одинаковых чисел поля 'Код_сотрудника' в таблице 'Номера'");
            this.toolTip4.SetToolTip(radioButton4, "Корректирует тариф у номера (устанавливает тариф 'Смарт'), оформленного определенным сотрудником (с кодом 2). Перед этим производится отбор номеров по введенному сотруднику.");
            this.toolTip5.SetToolTip(radioButton5, "Получение всех услуг, которые получил номер, соответствующий введенному коду");
            this.toolTip7.SetToolTip(radioButton7, "Получение средней суммы пополнения счета на номер, соответствующий введенному коду");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public static Boolean isNumber(String str)
        {
            int j;
            if (Int32.TryParse(str, out j))
                return true;
            else
                return false;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                this.textBox1.Visible = true;
                label1.Visible = true;
                label1.Text = "Введите код клиента";
                button1.Visible = true;
            }
            else
            {
                textBox1.Visible = false;
                label1.Visible = false;
                button1.Visible = false;
                dataGridView1.DataSource = null;
                label3.Visible = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlCommand sqlcmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            if (radioButton1.Checked)           // выполнение 1 задания
            {
                if (Procedures.isNumber(textBox1.Text))
                {
                    DataSet ds = new DataSet();
                    int code = Int16.Parse(textBox1.Text);
                                                                                        //тут планируется выбор из комбобокса клиентов, есть вопросы по однозначному определению кода выбранного клиента
                    /*da = new SqlDataAdapter("Select код, ФИО from Клиенты order by код", dataBaseConnection);
                    da.Fill(ds, "Клиенты_выбор");
                    for (int i=0; i < )
                    choise.Items.Add = ds.Tables["Клиенты_выбор"].Rows[i];*/

                    int checkCode = 0;
                    sqlcmd.CommandText = "Select код from Номера where код = " + code;
                    sqlcmd.Connection = dataBaseConnection;
                    da.SelectCommand = sqlcmd;
                    da.Fill(ds, "Номера");
                    checkCode = ds.Tables["Номера"].Rows.Count;
                    if (checkCode != 0)
                    {
                        label3.Visible = false;
                        sqlcmd.CommandText = "exec Владение_номерами " + code;
                        da.SelectCommand = sqlcmd;
                        da.Fill(ds, "exec1");
                        dataGridView1.DataSource = ds.Tables["exec1"];
                        label3.Visible = false;
                        dataGridView1.Update();
                    }
                    else
                    {
                        MessageBox.Show("Номер, соответствующий введенному коду не найден. Повторите ввод.", "Ошибка ввода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox1.Clear();
                        textBox1.Focus();
                        dataGridView1.DataSource = null;
                        label3.Visible = true;
                    }
                }
                else
                {
                    MessageBox.Show("Введено нецифровое значение. Повторите ввод.", "Некорректный ввод", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox1.Clear();
                    textBox1.Focus();
                }
                
            }

            if (radioButton2.Checked)       //выполнение 2 задания
            {
                DataSet ds = new DataSet();
                SqlDataAdapter dataAdapter;
                String Number = textBox1.Text;
                String Usluga = textBox3.Text;
                button1.Visible = true;
                if (Number == "")
                    Number = "NULL";
                if (Usluga == "")
                {
                    Usluga = "NULL";
                }
                label3.Visible = false;
                dataAdapter = new SqlDataAdapter("exec Услуги '" + Number + "', '" + Usluga + "'", dataBaseConnection);
                dataAdapter.Fill(ds, "Услуги");
                dataGridView1.DataSource = ds.Tables["Услуги"];
                dataGridView1.Update();
            }

            if (radioButton4.Checked)           //выполнение 4 задания
            {
                if (Procedures.isNumber(textBox1.Text))
                {
                    DataSet ds = new DataSet();
                    int code = Int32.Parse(textBox1.Text);
                    String sqlcmd1 = "exec Коррекция_тарифа " + code;
                    da = new SqlDataAdapter(sqlcmd1, dataBaseConnection);
                    da.Fill(ds, "Коррекция_тарифа");
                    dataGridView1.DataSource = ds.Tables["Коррекция_тарифа"];
                    dataGridView1.Update();
                    label3.Visible = false;
                }
                else
                {
                    MessageBox.Show("Введено нецифровое значение. Повторите ввод.", "Некорректный ввод", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox1.Clear();
                    textBox1.Focus();
                }
            }

            if (radioButton5.Checked)
            {
                if (Procedures.isNumber(textBox1.Text))
                {
                    DataSet ds = new DataSet();
                    String code = textBox1.Text;
                    String cmd = "exec Услуги_на_номер " + code;
                    da = new SqlDataAdapter(cmd, dataBaseConnection);
                    da.Fill(ds, "Услуги_на_номер");
                    dataGridView1.DataSource = ds.Tables["Услуги_на_номер"];
                    dataGridView1.Update();
                    label3.Visible = false;
                }
                else
                {
                    MessageBox.Show("Введено нецифровое значение. Повторите ввод.", "Некорректный ввод", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox1.Clear();
                    textBox1.Focus();
                }
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)    //2 задание
        {
            if (radioButton2.Checked)
            {
                label1.Text = "Ввод номера:";
                label1.Visible = true;
                label4.Visible = true;
                textBox3.Visible = true;
                textBox1.Visible = true;
                button1.Visible = true;
            }
            else
            {
                label3.Visible = true;
                dataGridView1.DataSource = null;
                label4.Visible = false;
                textBox3.Visible = false;
                textBox1.Visible = false;
                label1.Visible = false;
                button1.Visible = false;
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)    //3 задание
        {
            if (radioButton3.Checked)
            {
                SqlCommand sqlcmd = new SqlCommand();
                DataSet ds = new DataSet();                                                                        
                SqlDataAdapter da = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand("declare @res int; exec @res = Подпоследовательность; select 'max' = @res;");
                cmd.Connection = dataBaseConnection;
                da.SelectCommand = cmd;
                da.Fill(ds, "Результат");
                int result = 0;
                DataTable dt = new DataTable();  //создаем переменную под таблицу
                dt = ds.Tables["Результат"];  // получаем таблицу
                DataRow dr = dt.Rows[0];        //берем из таблицы первую (она же и единственная) строку
                result = (int)dr["max"];      //получаем оттуда имя пользователя
                MessageBox.Show("Лучший работник имеет " + result + " подряд подключенных номеров", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);  //собственно вывод результата
                radioButton3.Checked = false;
            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)    //4 задание
        {
            if (radioButton4.Checked)
            {
                label1.Visible = true;
                textBox1.Visible = true;
                button1.Visible = true;
                label1.Text = "Введите код сотрудника для отбор номеров:";
                label1.AutoSize = true;
            }
            else
            {
                label1.Visible = false;
                textBox1.Visible = false;
                button1.Visible = false;
                dataGridView1.DataSource = null;
                label3.Visible = true;
            }
        }

        private void Procedures_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                if (MessageBox.Show("Вы действительно хотите прекратить просмотр процедур?", "Подтверждение закрытия", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    this.Close();
            }
            else
                return;
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton5.Checked)
            {
                button1.Visible = true;
                label1.Visible = true;
                label1.Text = "Ввод кода номера:";
                textBox1.Visible = true;
            }
            else
            {
                button1.Visible = false;
                label1.Visible = false;
                textBox1.Visible = false;
                label3.Visible = true;
                dataGridView1.DataSource = null;
            }
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton7.Checked)
            {
                DataSet ds = new DataSet();
                String sqlcmd1 = "select код, номер, dbo.Среднее_пополнение(код) as Среднее_пополнение from Номера order by код";
                SqlDataAdapter  da = new SqlDataAdapter(sqlcmd1, dataBaseConnection);
                da.Fill(ds, "Среднее_пополнение");
                dataGridView1.DataSource = ds.Tables["Среднее_пополнение"];
                dataGridView1.Update();
                label3.Visible = false;
            }
            else
            {
                dataGridView1.DataSource = null;
                label3.Visible = true;
            }
        }

    }
}
