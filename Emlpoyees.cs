﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Programm
{
    public partial class Emlpoyees : Form
    {
        SqlConnection dataBaseConnection;
        DataSet ds;
        SqlDataAdapter da;
        String login;
        public Emlpoyees(SqlConnection sql, String user)
        {
            dataBaseConnection = sql;
            login = user;
            InitializeComponent();
        }

        private void Emlpoyees_Load(object sender, EventArgs e)
        {
            this.Width = 687;
            this.Height = 460;
            RefreshData();
            ControlBox = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Height = 639;
            RegistrationEmployee.Visible = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Height = 460;
            RegistrationEmployee.Visible = false;
            FIO.Clear();
            Dolzn.Clear();
        }

        private void RefreshData()      //обновление данных на форме
        {
            ds = new DataSet();
            String search = textBox1.Text;
            da = new SqlDataAdapter("Select ФИО, должность, код from Сотрудники where ФИО like '" + search + "%' order by ФИО", dataBaseConnection);
            da.Fill(ds, "Штат");
            dataGridView1.DataSource = ds.Tables["Штат"];
            dataGridView1.Columns[2].Visible = false;
            dataGridView1.Update();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (FIO.Text == "")
            {
                MessageBox.Show("Введите ФИО нового сотрудника", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (Dolzn.Text == "")
            {
                MessageBox.Show("Введите должность нового сотрудника", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            String fio = FIO.Text;      //получение фио клиента
            String dolzn = Dolzn.Text;  //получение должности
            SqlCommand cmd = new SqlCommand("Insert into Сотрудники values('" + fio + "', '" + dolzn + "')");
            cmd.Connection = dataBaseConnection;
            dataBaseConnection.Open();
            cmd.ExecuteNonQuery();
            dataBaseConnection.Close();
            DateTime time = DateTime.Today;
            SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + login + "', 'Принял на работу " + fio + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
            dataBaseConnection.Open();
            cmd1.ExecuteNonQuery();
            dataBaseConnection.Close();
            RegistrationEmployee.Visible = false;
            FIO.Clear();
            Dolzn.Clear();
            RefreshData();
            this.Height = 460;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы подтвержадаете выполнение действия? Его выполнение послечет за собой удаление всех упоминаний данного сотрудника в базе", "Подтверждение увольнения", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                int selectedCode = (int)dataGridView1["код", dataGridView1.CurrentCell.RowIndex].Value;
                String query = "";
                query += "Delete from Консультации where код_сотрудника = " + selectedCode + ";";
                query += "Delete from Продажа_телефонов where код_сотрудника = " + selectedCode + ";";
                query += "Delete from Пополнение where код_номера in (Select код from Номера where код_сотрудника = " + selectedCode + ")";
                query += "Delete from Полученные_услуги where код_номера in (Select код from Номера where код_сотрудника = " + selectedCode + ")";
                query += "Delete from Номера where код_сотрудника = " + selectedCode + ";";
                query += "Delete from Сотрудники where код = " + selectedCode;
                SqlCommand cmd = new SqlCommand(query, dataBaseConnection);
                dataBaseConnection.Open();
                cmd.ExecuteNonQuery();
                dataBaseConnection.Close();
                DateTime time = DateTime.Today;
                SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + login + "', 'Уволил сотрудника " + (String)dataGridView1["ФИО", dataGridView1.CurrentCell.RowIndex].Value + "', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                dataBaseConnection.Open();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                MessageBox.Show("Удаление выполнено. Все упоминания об этом сотруднике также были удалены.", "Успешное выполнение команды", MessageBoxButtons.OK, MessageBoxIcon.Information);
                RefreshData();
            }
            else
                return;
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
