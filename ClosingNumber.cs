﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Programm
{
    public partial class ClosingNumber : Form
    {
        SqlConnection dataBaseConnection;
        DataSet ds;
        int code;
        String login1;
        public ClosingNumber(SqlConnection sql, String lg)
        {
            login1 = lg;
            dataBaseConnection = sql;
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RefreshData()
        {
            ds = new DataSet();
            String search = textBox1.Text;
            SqlDataAdapter da = new SqlDataAdapter("Select Номера.номер, Клиенты.ФИО Владелец, дата_подключения as 'Дата подключения', Номера.код from Номера, Клиенты where Клиенты.код = Номера.код_владельца and Номера.номер like '" + search + "%' and дата_отключения is null order by номер", dataBaseConnection);
            da.Fill(ds, "Номера");
            dataGridView1.DataSource = ds.Tables["Номера"];
            dataGridView1.Columns[3].Visible = false;
            dataGridView1.Update();
        }

        private void ClosingNumber_Load(object sender, EventArgs e)
        {
            RefreshData();
            ControlBox = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ds = new DataSet();
            code = (int)dataGridView1["код", dataGridView1.CurrentCell.RowIndex].Value; //получили код номера под завершение обслуживания
            DateTime currentDate = DateTime.Today;  //получили сегодняшнюю дату
            DateTime dateReg;   //дата_подключения
            SqlCommand cmd = new SqlCommand("Select дата_подключения, код_тарифа from Номера where код = " + code);
            cmd.Connection = dataBaseConnection;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;         //получили дату в адаптер
            da.Fill(ds, "Результат");
            DataTable dt = new DataTable();
            dt = ds.Tables["Результат"];
            DataRow dr = dt.Rows[0];
            dateReg = (DateTime)dr["дата_подключения"];
            int colMonths = ((currentDate.Year - dateReg.Year) * 12) + currentDate.Month - dateReg.Month;  //посчитали разницу в месяцах между датами подключения и сегодняшней датой
            int codetar = (int)dr["код_тарифа"];            //получение кода тарифа для определения ежемесячного платежа
            SqlDataAdapter da1 = new SqlDataAdapter("Select абонплата from Тарифы where код = " + codetar, dataBaseConnection);
            da1.Fill(ds, "Стоимость тарифа");
            dt = ds.Tables["Стоимость тарифа"];
            dr = dt.Rows[0];
            Decimal SumDes = (Decimal)dr["абонплата"];      
            int Sum = (int)SumDes;      //получили сумму абонплаты
            Sum *= colMonths;           //считаем суммарный долг
            cmd.CommandText = "Select sum(сумма) as sum from пополнение where код_номера = " + code;
            da1.SelectCommand = cmd;        //выполнили запрос
            da.Fill(ds, "Оплачено");
            dt = ds.Tables["Оплачено"];
            dr = dt.Rows[0];
            int Opl;
            try
            {
                Decimal OplDes = (Decimal)dr["sum"];
                Opl = (int)OplDes;
            }
            catch (InvalidCastException)
            {
                Opl = 0;
            }
            if (Sum > Opl)
            {
                MessageBox.Show("В закрытии номера отказано. Причина - отрицательный лицевой баланс на номере. Операция будет разрешена после погашения долга.\n\n\n\nНа данный момент долг равен " + String.Format("{0:0,0}", (Sum-Opl)) + " рублей", "Отказ в проведении операции.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                cmd.CommandText = "Update Номера set дата_отключения = '" + currentDate + "' where код = " + code;
                dataBaseConnection.Open();
                cmd.ExecuteNonQuery();
                dataBaseConnection.Close();
                DateTime time = DateTime.Today;
                SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + login1 + "', 'Завершил обслуживание номера " + (String)dataGridView1["Номер", dataGridView1.CurrentCell.RowIndex].Value + "', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                dataBaseConnection.Open();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                MessageBox.Show("Операция выполнена. Номер неактивен.", "Операция выполнена успешно", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (MessageBox.Show("Желаете повторить операцию с другими номерами?", "Продолжение работы", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    textBox1.Clear();
                    textBox1.Focus();
                    return;
                }
                else
                    this.Close();
            }    
        }
    }
}
