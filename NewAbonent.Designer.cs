﻿namespace Programm
{
    partial class NewAbonent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewAbonent));
            this.button1 = new System.Windows.Forms.Button();
            this.labelNumber = new System.Windows.Forms.Label();
            this.textCountryCode = new System.Windows.Forms.TextBox();
            this.Code = new System.Windows.Forms.ComboBox();
            this.Number = new System.Windows.Forms.MaskedTextBox();
            this.Employee = new System.Windows.Forms.Label();
            this.Employees = new System.Windows.Forms.DataGridView();
            this.labelClient = new System.Windows.Forms.Label();
            this.Clients = new System.Windows.Forms.DataGridView();
            this.regClient = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.labelTarif = new System.Windows.Forms.Label();
            this.RegistrationClient = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.FIO = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Tariffs = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Employees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clients)).BeginInit();
            this.RegistrationClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Tariffs)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(842, 449);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 35);
            this.button1.TabIndex = 0;
            this.button1.Text = "Выход";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelNumber
            // 
            this.labelNumber.AutoSize = true;
            this.labelNumber.Location = new System.Drawing.Point(43, 33);
            this.labelNumber.Name = "labelNumber";
            this.labelNumber.Size = new System.Drawing.Size(79, 13);
            this.labelNumber.TabIndex = 1;
            this.labelNumber.Text = "Новый номер:";
            // 
            // textCountryCode
            // 
            this.textCountryCode.Enabled = false;
            this.textCountryCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textCountryCode.Location = new System.Drawing.Point(128, 27);
            this.textCountryCode.Name = "textCountryCode";
            this.textCountryCode.Size = new System.Drawing.Size(28, 23);
            this.textCountryCode.TabIndex = 2;
            this.textCountryCode.Text = "375";
            // 
            // Code
            // 
            this.Code.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Code.FormattingEnabled = true;
            this.Code.Items.AddRange(new object[] {
            "29",
            "44",
            "33",
            "25"});
            this.Code.Location = new System.Drawing.Point(162, 27);
            this.Code.Name = "Code";
            this.Code.Size = new System.Drawing.Size(38, 24);
            this.Code.TabIndex = 3;
            // 
            // Number
            // 
            this.Number.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Number.Location = new System.Drawing.Point(206, 28);
            this.Number.Mask = "000-00-00";
            this.Number.Name = "Number";
            this.Number.Size = new System.Drawing.Size(103, 23);
            this.Number.TabIndex = 5;
            // 
            // Employee
            // 
            this.Employee.AutoSize = true;
            this.Employee.Location = new System.Drawing.Point(43, 69);
            this.Employee.Name = "Employee";
            this.Employee.Size = new System.Drawing.Size(77, 13);
            this.Employee.TabIndex = 6;
            this.Employee.Text = "Регистратор: ";
            // 
            // Employees
            // 
            this.Employees.AllowUserToAddRows = false;
            this.Employees.AllowUserToDeleteRows = false;
            this.Employees.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Employees.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.Employees.Location = new System.Drawing.Point(126, 69);
            this.Employees.Name = "Employees";
            this.Employees.Size = new System.Drawing.Size(377, 143);
            this.Employees.TabIndex = 7;
            // 
            // labelClient
            // 
            this.labelClient.AutoSize = true;
            this.labelClient.Location = new System.Drawing.Point(512, 69);
            this.labelClient.Name = "labelClient";
            this.labelClient.Size = new System.Drawing.Size(100, 13);
            this.labelClient.TabIndex = 8;
            this.labelClient.Text = "Владелец номера:";
            // 
            // Clients
            // 
            this.Clients.AllowUserToAddRows = false;
            this.Clients.AllowUserToDeleteRows = false;
            this.Clients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Clients.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.Clients.Location = new System.Drawing.Point(618, 69);
            this.Clients.Name = "Clients";
            this.Clients.Size = new System.Drawing.Size(366, 143);
            this.Clients.TabIndex = 9;
            // 
            // regClient
            // 
            this.regClient.Location = new System.Drawing.Point(515, 113);
            this.regClient.Name = "regClient";
            this.regClient.Size = new System.Drawing.Size(97, 53);
            this.regClient.TabIndex = 10;
            this.regClient.Text = "Регистрация нового клиента";
            this.regClient.UseVisualStyleBackColor = true;
            this.regClient.Click += new System.EventHandler(this.regClient_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(314, 416);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(191, 68);
            this.button2.TabIndex = 11;
            this.button2.Text = "Зарегистрировать";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // labelTarif
            // 
            this.labelTarif.AutoSize = true;
            this.labelTarif.Location = new System.Drawing.Point(39, 310);
            this.labelTarif.Name = "labelTarif";
            this.labelTarif.Size = new System.Drawing.Size(83, 13);
            this.labelTarif.TabIndex = 12;
            this.labelTarif.Text = "Выбор тарифа:";
            // 
            // RegistrationClient
            // 
            this.RegistrationClient.Controls.Add(this.button4);
            this.RegistrationClient.Controls.Add(this.button3);
            this.RegistrationClient.Controls.Add(this.dateTimePicker1);
            this.RegistrationClient.Controls.Add(this.label2);
            this.RegistrationClient.Controls.Add(this.FIO);
            this.RegistrationClient.Controls.Add(this.label1);
            this.RegistrationClient.Location = new System.Drawing.Point(361, 257);
            this.RegistrationClient.Name = "RegistrationClient";
            this.RegistrationClient.Size = new System.Drawing.Size(604, 153);
            this.RegistrationClient.TabIndex = 14;
            this.RegistrationClient.TabStop = false;
            this.RegistrationClient.Text = "Регистрация нового клиента";
            this.RegistrationClient.Visible = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(509, 115);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "Отмена";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(186, 115);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(238, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Зарегистрировать нового клиента";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(207, 78);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(98, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Дата рождения:";
            // 
            // FIO
            // 
            this.FIO.Location = new System.Drawing.Point(207, 34);
            this.FIO.MaxLength = 45;
            this.FIO.Name = "FIO";
            this.FIO.Size = new System.Drawing.Size(377, 20);
            this.FIO.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Фамилия Имя Отчество:";
            // 
            // Tariffs
            // 
            this.Tariffs.AllowUserToAddRows = false;
            this.Tariffs.AllowUserToDeleteRows = false;
            this.Tariffs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Tariffs.Cursor = System.Windows.Forms.Cursors.Default;
            this.Tariffs.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.Tariffs.Location = new System.Drawing.Point(124, 270);
            this.Tariffs.Name = "Tariffs";
            this.Tariffs.Size = new System.Drawing.Size(155, 150);
            this.Tariffs.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 222);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(165, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Поиск - начните вводить ФИО:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(206, 215);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(199, 20);
            this.textBox1.TabIndex = 17;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(512, 222);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(165, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Поиск - начните вводить ФИО:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(685, 215);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(260, 20);
            this.textBox2.TabIndex = 19;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("a_AssuanTitulB&W", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(531, 473);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(286, 21);
            this.label11.TabIndex = 24;
            this.label11.Text = "Отдел работы с номерами";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("a_AvanteDrp", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.Green;
            this.label10.Location = new System.Drawing.Point(570, 410);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(215, 78);
            this.label10.TabIndex = 23;
            this.label10.Text = "Mobi";
            // 
            // NewAbonent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 506);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Tariffs);
            this.Controls.Add(this.RegistrationClient);
            this.Controls.Add(this.labelTarif);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.regClient);
            this.Controls.Add(this.Clients);
            this.Controls.Add(this.labelClient);
            this.Controls.Add(this.Employees);
            this.Controls.Add(this.Employee);
            this.Controls.Add(this.Number);
            this.Controls.Add(this.Code);
            this.Controls.Add(this.textCountryCode);
            this.Controls.Add(this.labelNumber);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NewAbonent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Отдел работы с номерами: регистрация нового номера";
            this.Load += new System.EventHandler(this.NewAbonent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Employees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clients)).EndInit();
            this.RegistrationClient.ResumeLayout(false);
            this.RegistrationClient.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Tariffs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelNumber;
        private System.Windows.Forms.TextBox textCountryCode;
        private System.Windows.Forms.ComboBox Code;
        private System.Windows.Forms.MaskedTextBox Number;
        private System.Windows.Forms.Label Employee;
        private System.Windows.Forms.DataGridView Employees;
        private System.Windows.Forms.Label labelClient;
        private System.Windows.Forms.DataGridView Clients;
        private System.Windows.Forms.Button regClient;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label labelTarif;
        private System.Windows.Forms.GroupBox RegistrationClient;
        private System.Windows.Forms.TextBox FIO;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridView Tariffs;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
    }
}