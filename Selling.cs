﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Programm
{
    public partial class Selling : Form
    {
        SqlConnection dataBaseConnection;
        bool searchFlag = true;         //флаг для очистки и заполнения поиска по сотрудникам
        bool search1Flag = true;        // --//-- для клиента
        DataSet ds;
        SqlDataAdapter clientsAdapter;  //для клиентов
        SqlDataAdapter emplAdapter;     //для сотрудников
        String login;
        public Selling(SqlConnection sql, String lg)
        {
            login = lg;
            dataBaseConnection = sql;
            InitializeComponent();
        }

        private void search_TextChanged(object sender, EventArgs e)
        {
            RefreshData();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String fio = FIO.Text;      //получение фио клиента
            String Date = dateTimePicker1.Value.ToString("yyyy-MM-dd");
            DateTime nowDate = DateTime.Today;                  //сегодняшняя дата
            String TimeReg = nowDate.ToString("yyyy-MM-dd");    //конвертирование под формат сервера
            SqlCommand cmd = new SqlCommand("Insert into Клиенты values('" + fio + "', '" + Date + "', '" + TimeReg + "')");
            cmd.Connection = dataBaseConnection;
            dataBaseConnection.Open();
            cmd.ExecuteNonQuery();
            dataBaseConnection.Close();
            RegistrationClient.Visible = false;
            FIO.Clear();
            RefreshData();
            this.Height = 457;
        }

        private void RefreshData()
        {
            ds = new DataSet();
            String search1 = Emp.Text;  //значение для поиска продавца
            String search2 = Cl.Text;   //значение для поиска покупателя
            emplAdapter = new SqlDataAdapter("Select ФИО, Должность, код as 'Код сотрудника' from Сотрудники where ФИО like '" + search1 + "%' order by ФИО", dataBaseConnection);       //получили сотрудников
            emplAdapter.Fill(ds, "Сотрудники");
            clientsAdapter = new SqlDataAdapter("SELECT ФИО, дата_рождения as 'Дата рождения', код 'Код клиента' FROM Клиенты where ФИО like '" + search2 + "%' order by ФИО", dataBaseConnection);       //получили клиентов
            clientsAdapter.Fill(ds, "Клиенты");
            seller.DataSource = ds.Tables["Сотрудники"]; //отображение сотрудников
            buyer.DataSource = ds.Tables["Клиенты"]; //отображение клиентов
            buyer.Columns[2].Visible = false;
            seller.Columns[2].Visible = false;
            buyer.Update();
            buyer.Update();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            RegistrationClient.Visible = false;
            FIO.Clear();
            this.Height = 457;
        }

        private void search1_TextChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void Selling_Load(object sender, EventArgs e)
        {
            RefreshData();
            searchFlag = true;
            search1Flag = true;
            this.Height = 457;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Height = 645;
            RegistrationClient.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int ClientCode = (int)buyer["Код клиента", buyer.CurrentCell.RowIndex].Value;   //получили код выбранного клиента
            String ClientName = (String)buyer["ФИО", buyer.CurrentCell.RowIndex].Value;     //ФИО для вывода
            String EmployeeName = (String)seller["ФИО", seller.CurrentCell.RowIndex].Value;                     //ФИО для вывода
            int EmployeeCode = (int)seller["Код сотрудника", seller.CurrentCell.RowIndex].Value;   //получение кода сотрудника для запроса
            String Message = "Подтвердите введенные данные:\n\n Модель:                      " + model.Text;
            Decimal sum = Decimal.Parse(maskedTextBox1.Text.Trim());
            Message += "\nФИО покупателя:      " + ClientName + "\nФИО продавца:          " + EmployeeName + "\nСумма:                         " + sum + "";
            if (MessageBox.Show(Message, "Подтверждение введенных данных.", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                DateTime nowDate = DateTime.Today;                  //сегодняшняя дата
                String TimeReg = nowDate.ToString("yyyy-MM-dd");    //конвертирование под формат сервера
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = dataBaseConnection;
                cmd.CommandText = "Insert into Продажа_телефонов values ('" + model.Text + "', " + sum + ", '" + TimeReg + "', " + EmployeeCode + ", " + ClientCode + ")";    //запись в базу
                dataBaseConnection.Open();
                cmd.ExecuteNonQuery();      //выполнение запроса
                dataBaseConnection.Close();
                DateTime time = DateTime.Today;
                SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + login + "', 'Оформил продажу', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                dataBaseConnection.Open();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                if (MessageBox.Show("Регистрация успешно завершена.\n\n\n\nЖелаете продолжить регистрацию?", "Успешное выполнение операции", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    RefreshData();  //обновление данных
                    model.Clear(); //очистка данных
                    maskedTextBox1.Text = ""; //то же самое
                    RegistrationClient.Visible = false;
                    model.Focus();
                }
                else
                {
                    this.Close();
                }        
            }
         }
     }
}
