﻿namespace Programm
{
    partial class Selling
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Selling));
            this.label1 = new System.Windows.Forms.Label();
            this.model = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.seller = new System.Windows.Forms.DataGridView();
            this.Emp = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.RegistrationClient = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.FIO = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.buyer = new System.Windows.Forms.DataGridView();
            this.Cl = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.seller)).BeginInit();
            this.RegistrationClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buyer)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Модель телефона:";
            // 
            // model
            // 
            this.model.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.model.Location = new System.Drawing.Point(145, 34);
            this.model.MaxLength = 40;
            this.model.Name = "model";
            this.model.Size = new System.Drawing.Size(173, 23);
            this.model.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Стоимость:";
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maskedTextBox1.Location = new System.Drawing.Point(145, 80);
            this.maskedTextBox1.Mask = "0000000000000000000";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(173, 23);
            this.maskedTextBox1.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(324, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "руб.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(38, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Продавец:";
            // 
            // seller
            // 
            this.seller.AllowUserToAddRows = false;
            this.seller.AllowUserToDeleteRows = false;
            this.seller.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.seller.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.seller.Location = new System.Drawing.Point(145, 136);
            this.seller.Name = "seller";
            this.seller.Size = new System.Drawing.Size(225, 195);
            this.seller.TabIndex = 6;
            // 
            // Emp
            // 
            this.Emp.Location = new System.Drawing.Point(145, 347);
            this.Emp.Name = "Emp";
            this.Emp.Size = new System.Drawing.Size(225, 20);
            this.Emp.TabIndex = 7;
            this.Emp.TextChanged += new System.EventHandler(this.search_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(640, 376);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 36);
            this.button1.TabIndex = 8;
            this.button1.Text = "Закрыть";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(285, 376);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(194, 36);
            this.button2.TabIndex = 9;
            this.button2.Text = "Зафиксировать факт продажи";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(401, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Покупатель";
            // 
            // RegistrationClient
            // 
            this.RegistrationClient.Controls.Add(this.button4);
            this.RegistrationClient.Controls.Add(this.button3);
            this.RegistrationClient.Controls.Add(this.dateTimePicker1);
            this.RegistrationClient.Controls.Add(this.label6);
            this.RegistrationClient.Controls.Add(this.FIO);
            this.RegistrationClient.Controls.Add(this.label7);
            this.RegistrationClient.Location = new System.Drawing.Point(41, 428);
            this.RegistrationClient.Name = "RegistrationClient";
            this.RegistrationClient.Size = new System.Drawing.Size(604, 153);
            this.RegistrationClient.TabIndex = 15;
            this.RegistrationClient.TabStop = false;
            this.RegistrationClient.Text = "Регистрация нового клиента";
            this.RegistrationClient.Visible = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(509, 115);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "Отмена";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(186, 115);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(238, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Зарегистрировать нового клиента";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(207, 78);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(98, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Дата рождения:";
            // 
            // FIO
            // 
            this.FIO.Location = new System.Drawing.Point(207, 34);
            this.FIO.MaxLength = 45;
            this.FIO.Name = "FIO";
            this.FIO.Size = new System.Drawing.Size(377, 20);
            this.FIO.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(53, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(134, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Фамилия Имя Отчество:";
            // 
            // buyer
            // 
            this.buyer.AllowUserToAddRows = false;
            this.buyer.AllowUserToDeleteRows = false;
            this.buyer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.buyer.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.buyer.Location = new System.Drawing.Point(474, 136);
            this.buyer.Name = "buyer";
            this.buyer.Size = new System.Drawing.Size(252, 195);
            this.buyer.TabIndex = 16;
            // 
            // Cl
            // 
            this.Cl.Location = new System.Drawing.Point(474, 347);
            this.Cl.Name = "Cl";
            this.Cl.Size = new System.Drawing.Size(252, 20);
            this.Cl.TabIndex = 17;
            this.Cl.TextChanged += new System.EventHandler(this.search1_TextChanged);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(376, 161);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(89, 50);
            this.button5.TabIndex = 18;
            this.button5.Text = "Регистрация покупателя";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(55, 350);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Поиск по ФИО:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(381, 350);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Поиск по ФИО:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("a_AvanteDrp", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.Green;
            this.label10.Location = new System.Drawing.Point(481, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(215, 78);
            this.label10.TabIndex = 21;
            this.label10.Text = "Mobi";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("a_AssuanTitulB&W", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(489, 73);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(197, 27);
            this.label11.TabIndex = 22;
            this.label11.Text = "отдел продаж";
            // 
            // Selling
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(738, 591);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.Cl);
            this.Controls.Add(this.buyer);
            this.Controls.Add(this.RegistrationClient);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Emp);
            this.Controls.Add(this.seller);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.maskedTextBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.model);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Selling";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Отдел продаж: регистрация новой продажи";
            this.Load += new System.EventHandler(this.Selling_Load);
            ((System.ComponentModel.ISupportInitialize)(this.seller)).EndInit();
            this.RegistrationClient.ResumeLayout(false);
            this.RegistrationClient.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buyer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox model;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView seller;
        private System.Windows.Forms.TextBox Emp;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox RegistrationClient;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox FIO;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView buyer;
        private System.Windows.Forms.TextBox Cl;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }
}