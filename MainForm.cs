﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Programm
{
    public partial class MainForm : Form
    {
        SqlConnection dataBaseConnection;
        SqlDataAdapter dataAdapter;
        String currentTable;
        String UserName;
        int Rights;
        public MainForm(SqlConnection sql, int rights, String UserName)
        {
            Rights = rights;
            dataBaseConnection = sql;
            this.UserName = UserName;
            InitializeComponent();
            ControlBox = false;
        }

        #region Меню
        private void клиентыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 845;
            this.Height = 549;
            DataSet ds = new DataSet();         //сюда сохраняются таблицы из запросов (дописываются)
            dataAdapter = new SqlDataAdapter("SELECT ФИО, дата_рождения as 'Дата рождения', дата_регистрации as 'Дата регистрации', код  FROM Клиенты", dataBaseConnection);
            dataAdapter.Fill(ds, "Клиенты");
            ShowData.DataSource = ds.Tables["Клиенты"];
            int count = ds.Tables["Клиенты"].Rows.Count;
            currentTable = "Клиенты";
            SetVisiblesForTables(count, "Клиенты");
            ShowData.Columns[3].Visible = false;
            if (count == 0)
            {
                DeleteRecord.Visible = false;
            }
            else
            {
                if (Rights == 5)
                    DeleteRecord.Visible = true;
            }
        }

        private void консультацииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 845;
            this.Height = 549;
            DataSet ds = new DataSet();         //сюда сохраняются таблицы из запросов (дописываются)
            dataAdapter = new SqlDataAdapter("Select Сотрудники.ФИО as Сотрудник, Клиенты.ФИО as Клиент, Типы_операций.Тип as Операция, дата_обращения as Дата, Консультации.код from Консультации, Сотрудники, Клиенты, Типы_операций where код_сотрудника=Сотрудники.код and код_клиента = Клиенты.код and код_типа_операции = Типы_операций.код", dataBaseConnection); 
            dataAdapter.Fill(ds, "Консультации");
            ShowData.DataSource = ds.Tables["Консультации"];
            int count = ds.Tables["Консультации"].Rows.Count;
            SetVisiblesForTables(count, "Проведенные консультации");
            currentTable = "Консультации";
            ShowData.Columns[4].Visible = false; 
            if (count == 0)
            {
                DeleteRecord.Visible = false;
            }
            else
            {
                if (Rights == 5)
                    DeleteRecord.Visible = true;
            }
        }

        private void номераToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 1000;
            this.Height = 549;
            DataSet ds = new DataSet();         //сюда сохраняются таблицы из запросов (дописываются)
            dataAdapter = new SqlDataAdapter("Select Номер, Сотрудники.ФИО as Подключивший, Клиенты.ФИО as Владелец, dbo.Среднее_пополнение(Номера.код) as 'Среднее пополнение',  Тарифы.Название as Тариф, дата_подключения as 'Дата подключения', дата_отключения as 'Дата отключения', Номера.код from Номера, Сотрудники, Клиенты, Тарифы where код_сотрудника=Сотрудники.код and код_владельца=Клиенты.код and код_тарифа = Тарифы.код", dataBaseConnection);
            dataAdapter.Fill(ds, "Номера");
            ShowData.DataSource = ds.Tables["Номера"];
            int count = ds.Tables["Номера"].Rows.Count;
            SetVisiblesForTables(count, "Номера");
            currentTable = "Номера";
            ShowData.Columns[7].Visible = false;
            if (count == 0)
            {
                DeleteRecord.Visible = false;
            }
            else
            {
                if (Rights == 5 || Rights == 1)
                    DeleteRecord.Visible = true;
            }
        }

        private void полученныеуслугиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 845;
            this.Height = 549;
            DataSet ds = new DataSet();         //сюда сохраняются таблицы из запросов (дописываются)
            dataAdapter = new SqlDataAdapter("Select Тип_услуги.название as 'Тип услуги', Номера.Номер, Количество, дата_предоставления as 'Дата предоставления', Полученные_услуги.код from Полученные_услуги, Номера, Тип_услуги where Номера.код = код_номера and код_типа_услуги = Тип_услуги.код", dataBaseConnection);
            dataAdapter.Fill(ds, "Полученные_услуги");
            ShowData.DataSource = ds.Tables["Полученные_услуги"];
            int count = ds.Tables["Полученные_услуги"].Rows.Count;
            SetVisiblesForTables(count, "Информация о предоставленных услугах");
            currentTable = "Полученные_услуги";
            ShowData.Columns[4].Visible = false;
            if (count == 0)
            {
                DeleteRecord.Visible = false;
            }
            else
            {
                if (Rights == 5)
                    DeleteRecord.Visible = true;
            }
        }

        private void пополнениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 845;
            this.Height = 549;
            DataSet ds = new DataSet();         //сюда сохраняются таблицы из запросов (дописываются)
            dataAdapter = new SqlDataAdapter("Select Номера.Номер, Сумма, дата_пополнения as 'Дата пополнения', Пополнение.код from Пополнение, Номера where код_номера=Номера.код", dataBaseConnection);
            dataAdapter.Fill(ds, "Пополнение");
            ShowData.DataSource = ds.Tables["Пополнение"];
            int count = ds.Tables["Пополнение"].Rows.Count;
            SetVisiblesForTables(count, "Информация о пополнении счета");
            currentTable = "Пополнение";
            ShowData.Columns[3].Visible = false;
            if (count == 0)
            {
                DeleteRecord.Visible = false;
            }
            else
            {
                if (Rights == 5)
                    DeleteRecord.Visible = true;
            }
        }

        private void продажаТелефоновToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 845;
            this.Height = 549;
            DataSet ds = new DataSet();         //сюда сохраняются таблицы из запросов (дописываются)
            dataAdapter = new SqlDataAdapter("Select Модель, Стоимость, дата_продажи 'Дата продажи', Сотрудники.ФИО as Продавец, Клиенты.ФИО as Покупатель, Продажа_телефонов.код from Продажа_телефонов, Сотрудники, Клиенты where код_сотрудника = Сотрудники.код and код_покупателя = Клиенты.код", dataBaseConnection);
            dataAdapter.Fill(ds, "Продажа_телефонов");
            ShowData.DataSource = ds.Tables["Продажа_телефонов"];
            int count = ds.Tables["Продажа_телефонов"].Rows.Count;
            SetVisiblesForTables(count, "Информация о продажах телефонов");
            currentTable = "Продажа_телефонов";
            ShowData.Columns[5].Visible = false;
            if (count == 0)
            {
                DeleteRecord.Visible = false;
            }
            else
            {
                if (Rights == 5 || Rights == 2)
                    DeleteRecord.Visible = true;
            }
        }

        private void ремонтToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 845;
            this.Height = 549;
            DataSet ds = new DataSet();         //сюда сохраняются таблицы из запросов (дописываются)
            dataAdapter = new SqlDataAdapter("Select Клиенты.ФИО as Клиент, дата_приема as 'Дата приема', Ремонт.код from Ремонт, Клиенты where Клиенты.код = код_клиента", dataBaseConnection);
            dataAdapter.Fill(ds, "Ремонт");
            ShowData.DataSource = ds.Tables["Ремонт"];
            int count = ds.Tables["Ремонт"].Rows.Count;
            SetVisiblesForTables(count, "Информация о ремонте");
            currentTable = "Ремонт";
            ShowData.Columns[2].Visible = false;
            if (count == 0)
            {
                DeleteRecord.Visible = false;
            }
            else
            {
                if (Rights == 5)
                    DeleteRecord.Visible = true;
            }
        }

        private void сотрудникиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 845;
            this.Height = 549;
            DataSet ds = new DataSet();         //сюда сохраняются таблицы из запросов (дописываются)
            dataAdapter = new SqlDataAdapter("Select ФИО, Должность, код from Сотрудники", dataBaseConnection);
            dataAdapter.Fill(ds, "Сотрудники");
            ShowData.DataSource = ds.Tables["Сотрудники"];
            int count = ds.Tables["Сотрудники"].Rows.Count;
            SetVisiblesForTables(count, "Сотрудники компании");
            currentTable = "Сотрудники";
            ShowData.Columns[2].Visible = false;
            if (count == 0)
            {
                DeleteRecord.Visible = false;
            }
            else
            {
                if (Rights == 5 || Rights == 3)
                    DeleteRecord.Visible = true;
            }
        }

        private void тарифыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 845;
            this.Height = 549;
            DataSet ds = new DataSet();         //сюда сохраняются таблицы из запросов (дописываются)
            dataAdapter = new SqlDataAdapter("Select  Название, Абонплата, код from Тарифы", dataBaseConnection);
            dataAdapter.Fill(ds, "Тарифы");
            ShowData.DataSource = ds.Tables["Тарифы"];
            int count = ds.Tables["Тарифы"].Rows.Count;
            SetVisiblesForTables(count, "Текущие тарифы");
            currentTable = "Тарифы";
            ShowData.Columns[2].Visible = false;
            if (count == 0)
            {
                DeleteRecord.Visible = false;
            }
            else
            {
                if (Rights == 5 || Rights == 4)
                    DeleteRecord.Visible = true;
            }
        }

        private void типыУслугToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 845;
            this.Height = 549;
            DataSet ds = new DataSet();         //сюда сохраняются таблицы из запросов (дописываются)
            dataAdapter = new SqlDataAdapter("Select  Название, Описание, Стоимость as 'Стоимость/ед', код from Тип_услуги", dataBaseConnection);
            dataAdapter.Fill(ds, "Тип_услуги");
            ShowData.DataSource = ds.Tables["Тип_услуги"];
            int count = ds.Tables["Тип_услуги"].Rows.Count;
            SetVisiblesForTables(count, "Предоставляемые услуги");
            currentTable = "Тип_услуги";
            ShowData.Columns[3].Visible = false;
            if (count == 0)
            {
                DeleteRecord.Visible = false;
            }
            else
            {
                if (Rights == 5)
                    DeleteRecord.Visible = true;
            }
        }

        private void типыОперацийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 845;
            this.Height = 549;
            DataSet ds = new DataSet();         //сюда сохраняются таблицы из запросов (дописываются)
            dataAdapter = new SqlDataAdapter("Select тип Название, Описание, код from Типы_операций", dataBaseConnection);
            dataAdapter.Fill(ds, "Типы_операций");
            ShowData.DataSource = ds.Tables["Типы_операций"];
            int count = ds.Tables["Типы_операций"].Rows.Count;
            SetVisiblesForTables(count, "Проводимые операции");
            currentTable = "Типы_операций";
            ShowData.Columns[2].Visible = false;
            if (count == 0)
            {
                DeleteRecord.Visible = false;
            }
            else
            {
                if (Rights == 5)
                    DeleteRecord.Visible = true;
            }
        }



        private void дополнительнаяИфнормацияToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void выходToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите завершить работу приложения?", "Подтверждение выхода", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                DateTime time = DateTime.Today;
                SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + UserName + "', 'Выход из аккаунта', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                dataBaseConnection.Open();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                Application.Exit();
            }
            else
                return;
        }

        private void программаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Курсовая работа Шибалова Романа, студента группы ПО-32\n2016 год", "О программе", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void абонентыToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Width = 845;
            this.Height = 549;
            DataSet ds = new DataSet();         //сюда сохраняются таблицы из запросов (дописываются)
            DeleteRecord.Visible = false;
            ShowData.Visible = true;
            ShowData.AutoSize = true;      //автоматический размер у грида
            Info.Text = "Действующие абоненты";
            Info.Visible = true;
            dataAdapter = new SqlDataAdapter("Select Номер, Подключивший, Владелец, Тариф, дата_подключения as 'Дата подключения', дата_отключения as 'Дата_отключения' from Абоненты", dataBaseConnection);
            dataAdapter.Fill(ds, "Абоненты");
            ShowData.DataSource = ds.Tables["Абоненты"];
            ShowData.Update();
            currentTable = "Абоненты";
            label1.Visible = false;
            label2.Visible = false;
            button1.Visible = true;
            this.Invalidate();          //перерисовка формы
        }

        private void логИзмененияБазыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Rights == 5)
            {
                Log log = new Log(dataBaseConnection);
                log.ShowDialog();
            }
            else
            {
                MessageBox.Show("Просмотр данной ифнормации доступен только администратору", "Ошибка доступа", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void admPanel_Click(object sender, EventArgs e)
        {
            AdminPanel adm = new AdminPanel(dataBaseConnection, UserName);
            adm.ShowDialog();
        }
        #endregion

        #region Удаление
        private void DeleteRecord_Click(object sender, EventArgs e)
        {
            int selectedCode = (int)ShowData["код", ShowData.CurrentCell.RowIndex].Value;    //получаем код записи для удаления

            if (MessageBox.Show("Вы действительно хотите удалить выделенную запись?\n\n\n Удаление записи может повлечь за собой удаление всех упоминаний о ней.", "Подтверждение удаления", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                DataSet ds = new DataSet();
                String query = "";
                int count;
                DateTime time;
                SqlCommand cmd1;
                switch (currentTable)
                {
                    case "Клиенты":
                         query = "";
                         query+="Delete from Продажа_телефонов where код_покупателя = " + selectedCode + ";";
                         query+="Delete from Консультации where код_клиента = " + selectedCode + ";";
                         query+="Delete from Ремонт where код_клиента = " + selectedCode + ";";
                         query+="Delete from Пополнение where код_номера in (Select код from Номера where код_владельца = " + selectedCode + ")";
                         query+="Delete from Полученные_услуги where код_номера in (Select код from Номера where код_владельца = " + selectedCode + ")";
                         query+="Delete from Номера where код_владельца = " + selectedCode + ";";
                         query+="Delete from " + currentTable + " where код = " + selectedCode;
                         time = DateTime.Today;
                         cmd1 = new SqlCommand("Insert into LogChanges values ('" + UserName + "', 'Удалил запись о клиенте " + ShowData["ФИО", ShowData.CurrentCell.RowIndex].Value + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                         dataBaseConnection.Open();
                         cmd1.ExecuteNonQuery();
                         dataBaseConnection.Close();
                         ExecuteQuery(query);
                         MessageBox.Show("Удаление выполнено. Все упоминания об удаленном клиенте также были удалены.", "Успешное выполнение команды", MessageBoxButtons.OK, MessageBoxIcon.Information);
                         dataAdapter = new SqlDataAdapter("SELECT ФИО, дата_рождения as 'Дата рождения', дата_регистрации as 'Дата регистрации', код FROM Клиенты", dataBaseConnection);
                         dataAdapter.Fill(ds, "Клиенты");
                         ShowData.DataSource = ds.Tables["Клиенты"];
                         ShowData.Update();
                         this.Invalidate();
                         count = ds.Tables["Клиенты"].Rows.Count;
                         if (count == 0)
                         {
                             DeleteRecord.Visible = false;
                         }
                         else
                         {
                             DeleteRecord.Visible = true;
                         }
                         ShowData.Columns[3].Visible = false;
                         break;
                    case "Консультации":
                         query = "";
                         query += "Delete from Консультации where код = " + selectedCode;
                         time = DateTime.Today;
                         cmd1 = new SqlCommand("Insert into LogChanges values ('" + UserName + "', 'Удалил запись о консультации от " + ShowData["Дата", ShowData.CurrentCell.RowIndex].Value.ToString() + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                         dataBaseConnection.Open();
                         cmd1.ExecuteNonQuery();
                         dataBaseConnection.Close();
                         ExecuteQuery(query);
                         MessageBox.Show("Удаление выполнено.", "Успешное выполнение команды", MessageBoxButtons.OK, MessageBoxIcon.Information);
                         dataAdapter = new SqlDataAdapter("Select Сотрудники.ФИО as Сотрудник, Клиенты.ФИО as Клиент, Типы_операций.Тип as Операция, дата_обращения as Дата, Консультации.код from Консультации, Сотрудники, Клиенты, Типы_операций where код_сотрудника=Сотрудники.код and код_клиента = Клиенты.код and код_типа_операции = Типы_операций.код", dataBaseConnection);
                         dataAdapter.Fill(ds, "Консультации");
                         ShowData.DataSource = ds.Tables["Консультации"];
                         ShowData.Update();                         
                         this.Invalidate();
                         count = ds.Tables["Консультации"].Rows.Count;
                         if (count == 0)
                         {
                             DeleteRecord.Visible = false;
                         }
                         else
                         {
                             DeleteRecord.Visible = true;
                         }
                         ShowData.Columns[4].Visible = false;
                         break;
                    case "Номера":
                        query = "";
                        query += "Delete from Пополнение where код_номера = " + selectedCode + ";";
                        query += "Delete from Полученные_услуги where код_номера = " + selectedCode + ";";
                        query += "Delete from Номера where код = " + selectedCode;
                        time = DateTime.Today;
                        cmd1 = new SqlCommand("Insert into LogChanges values ('" + UserName + "', 'Удалил запись о номере " + ShowData["Номер", ShowData.CurrentCell.RowIndex].Value.ToString() + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                        dataBaseConnection.Open();
                        cmd1.ExecuteNonQuery();
                        dataBaseConnection.Close();
                        ExecuteQuery(query);
                        MessageBox.Show("Удаление выполнено. Все упоминания удаленного номера также были удалены.", "Успешное выполнение команды", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dataAdapter = new SqlDataAdapter("Select номер as Номер, Сотрудники.ФИО as Подключивший, Клиенты.ФИО as Владелец, dbo.Среднее_пополнение(Номера.код) as 'Среднее пополнение',  Тарифы.Название as Тариф, дата_подключения as 'Дата подключения', дата_отключения as 'Дата отключения', Номера.код from Номера, Сотрудники, Клиенты, Тарифы where код_сотрудника=Сотрудники.код and код_владельца=Клиенты.код and код_тарифа = Тарифы.код", dataBaseConnection);
                        dataAdapter.Fill(ds, "Номера");
                        ShowData.DataSource = ds.Tables["Номера"];
                        count = ds.Tables["Номера"].Rows.Count;
                        if (count == 0)
                        {
                            DeleteRecord.Visible = false;
                        }
                        else
                        {
                            DeleteRecord.Visible = true;
                        }
                        ShowData.Update();
                        this.Invalidate();
                        ShowData.Columns[7].Visible = false;
                        break;
                    case "Полученные_услуги":
                        query = "";
                        query += "Delete from Полученные_услуги where код = " + selectedCode;
                        time = DateTime.Today;
                        cmd1 = new SqlCommand("Insert into LogChanges values ('" + UserName + "', 'Удалил запись об услуге " + ShowData["Тип услуги", ShowData.CurrentCell.RowIndex].Value.ToString() + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                        dataBaseConnection.Open();
                        cmd1.ExecuteNonQuery();
                        dataBaseConnection.Close();
                        ExecuteQuery(query);
                        MessageBox.Show("Удаление выполнено.", "Успешное выполнение команды", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dataAdapter = new SqlDataAdapter("Select Тип_услуги.название as 'Тип услуги', Номера.номер, Количество, дата_предоставления as 'Дата предоставления', Полученные_услуги.код from Полученные_услуги, Номера, Тип_услуги where Номера.код = код_номера and код_типа_услуги = Тип_услуги.код", dataBaseConnection);
                        dataAdapter.Fill(ds, "Полученные_услуги");
                        ShowData.DataSource = ds.Tables["Полученные_услуги"];                        
                        ShowData.Update();
                        count = ds.Tables["Полученные_услуги"].Rows.Count;
                        if (count == 0)
                        {
                            DeleteRecord.Visible = false;
                        }
                        else
                        {
                            DeleteRecord.Visible = true;
                        }
                        this.Invalidate();
                        ShowData.Columns[4].Visible = false;
                        break;
                    case "Пополнение":
                        query = "";
                        query += "Delete from Пополнение where код = " + selectedCode;
                        time = DateTime.Today;
                        cmd1 = new SqlCommand("Insert into LogChanges values ('" + UserName + "', 'Удалил запись о пополнении счета от " + ShowData["Дата пополнения", ShowData.CurrentCell.RowIndex].Value.ToString() + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                        dataBaseConnection.Open();
                        cmd1.ExecuteNonQuery();
                        dataBaseConnection.Close();
                        ExecuteQuery(query);
                        MessageBox.Show("Удаление выполнено.", "Успешное выполнение команды", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dataAdapter = new SqlDataAdapter("Select Номера.Номер, сумма, дата_пополнения as 'Дата пополнения', Пополнение.код from Пополнение, Номера where код_номера=Номера.код", dataBaseConnection);
                        dataAdapter.Fill(ds, "Пополнение");
                        ShowData.DataSource = ds.Tables["Пополнение"];  
                        count = ds.Tables["Пополнение"].Rows.Count;
                        if (count == 0)
                        {
                            DeleteRecord.Visible = false;
                        }
                        else
                        {
                            DeleteRecord.Visible = true;
                        }
                        ShowData.Update();
                        this.Invalidate();
                        ShowData.Columns[3].Visible = false;
                        break;
                    case "Продажа_телефонов":
                        query = "";
                        query += "Delete from Продажа_телефонов where код = " + selectedCode;
                        time = DateTime.Today;
                        cmd1 = new SqlCommand("Insert into LogChanges values ('" + UserName + "', 'Удалил запись о продаже телефона " + ShowData["Модель", ShowData.CurrentCell.RowIndex].Value.ToString() + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                        dataBaseConnection.Open();
                        cmd1.ExecuteNonQuery();
                        dataBaseConnection.Close();
                        ExecuteQuery(query);
                        MessageBox.Show("Удаление выполнено.", "Успешное выполнение команды", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dataAdapter = new SqlDataAdapter("Select Модель, Стоимость, дата_продажи 'Дата продажи', Сотрудники.ФИО as Продавец, Клиенты.ФИО as Покупатель, Продажа_телефонов.код from Продажа_телефонов, Сотрудники, Клиенты where код_сотрудника = Сотрудники.код and код_покупателя = Клиенты.код", dataBaseConnection);
                        dataAdapter.Fill(ds, "Продажа_телефонов");
                        ShowData.DataSource = ds.Tables["Продажа_телефонов"]; 
                        count = ds.Tables["Продажа_телефонов"].Rows.Count;
                        if (count == 0)
                        {
                            DeleteRecord.Visible = false;
                        }
                        else
                        {
                            DeleteRecord.Visible = true;
                        }
                        ShowData.Update();
                        this.Invalidate();
                        ShowData.Columns[5].Visible = false;
                        break;
                    case "Ремонт":
                        query = "";
                        query += "Delete from Ремонт where код = " + selectedCode;
                        time = DateTime.Today;
                        cmd1 = new SqlCommand("Insert into LogChanges values ('" + UserName + "', 'Удалил запись о ремонте от " + ShowData["Дата приема", ShowData.CurrentCell.RowIndex].Value.ToString() + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                        dataBaseConnection.Open();
                        cmd1.ExecuteNonQuery();
                        dataBaseConnection.Close();
                        ExecuteQuery(query);
                        MessageBox.Show("Удаление выполнено.", "Успешное выполнение команды", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dataAdapter = new SqlDataAdapter("Select  Клиенты.ФИО as Клиент, дата_приема as 'Дата приема', Ремонт.код from Ремонт, Клиенты where Клиенты.код = код_клиента", dataBaseConnection);
                        dataAdapter.Fill(ds, "Ремонт");
                        ShowData.DataSource = ds.Tables["Ремонт"];   
                        count = ds.Tables["Ремонт"].Rows.Count;
                        if (count == 0)
                        {
                            DeleteRecord.Visible = false;
                        }
                        else
                        {
                            DeleteRecord.Visible = true;
                        }
                        ShowData.Update();
                        this.Invalidate();
                        ShowData.Columns[2].Visible = false;
                        break;
                    case "Сотрудники":
                        query = "";
                        query += "Delete from Консультации where код_сотрудника = " + selectedCode + ";";
                        query += "Delete from Продажа_телефонов where код_сотрудника = " + selectedCode + ";";
                        query += "Delete from Пополнение where код_номера in (Select код from Номера where код_сотрудника = " + selectedCode + ")";
                        query += "Delete from Полученные_услуги where код_номера in (Select код from Номера where код_сотрудника = " + selectedCode + ")";
                        query += "Delete from Номера where код_сотрудника = " + selectedCode + ";";
                        query += "Delete from Сотрудники where код = " + selectedCode;
                        time = DateTime.Today;
                        cmd1 = new SqlCommand("Insert into LogChanges values ('" + UserName + "', 'Удалил запись о сотруднике " + ShowData["ФИО", ShowData.CurrentCell.RowIndex].Value.ToString() + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                        dataBaseConnection.Open();
                        cmd1.ExecuteNonQuery();
                        dataBaseConnection.Close();
                        ExecuteQuery(query);
                        MessageBox.Show("Удаление выполнено. Все упоминания об этом сотруднике также были удалены.", "Успешное выполнение команды", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dataAdapter = new SqlDataAdapter("Select ФИО, Должность, код from Сотрудники", dataBaseConnection);
                        dataAdapter.Fill(ds, "Сотрудники");
                        ShowData.DataSource = ds.Tables["Сотрудники"];
                        count = ds.Tables["Сотрудники"].Rows.Count;
                        if (count == 0)
                        {
                            DeleteRecord.Visible = false;
                        }
                        else
                        {
                            DeleteRecord.Visible = true;
                        }
                        ShowData.Update();
                        this.Invalidate();
                        ShowData.Columns[2].Visible = false;
                        break;
                    case "Тарифы":
                        query = "";
                        query += "Delete from Пополнение where код_номера in (select код from Номера where код_тарифа = " + selectedCode + ");";
                        query += "Delete from Полученные_услуги where код_номера in (select код from Номера where код_тарифа = " + selectedCode + ");";
                        query += "Delete from Номера where код_тарифа = " + selectedCode + ";";
                        query += "Delete from Тарифы where код = " + selectedCode;
                        time = DateTime.Today;
                        cmd1 = new SqlCommand("Insert into LogChanges values ('" + UserName + "', 'Удалил запись о тарифе " + ShowData["Название", ShowData.CurrentCell.RowIndex].Value.ToString() + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                        dataBaseConnection.Open();
                        cmd1.ExecuteNonQuery();
                        dataBaseConnection.Close();
                        ExecuteQuery(query);
                        MessageBox.Show("Удаление выполнено. Все упоминания данного тарифа также удалены.", "Успешное выполнение команды", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dataAdapter = new SqlDataAdapter("Select  Название, Абонплата, код from Тарифы", dataBaseConnection);
                        dataAdapter.Fill(ds, "Тарифы");
                        ShowData.DataSource = ds.Tables["Тарифы"];
                        count = ds.Tables["Тарифы"].Rows.Count;
                        if (count == 0)
                        {
                            DeleteRecord.Visible = false;
                        }
                        else
                        {
                            DeleteRecord.Visible = true;
                        }
                        ShowData.Update();
                        this.Invalidate();
                        ShowData.Columns[2].Visible = false;
                        break;
                    case "Тип_услуги":
                        query = "";
                        query += "Delete from Полученные_услуги where код_типа_услуги = " + selectedCode + ";";
                        query += "Delete from Тип_услуги where код = " + selectedCode;
                        time = DateTime.Today;
                        cmd1 = new SqlCommand("Insert into LogChanges values ('" + UserName + "', 'Удалил запись о типе услуги  " + ShowData["Название", ShowData.CurrentCell.RowIndex].Value.ToString() + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                        dataBaseConnection.Open();
                        cmd1.ExecuteNonQuery();
                        dataBaseConnection.Close();
                        ExecuteQuery(query);
                        MessageBox.Show("Удаление выполнено. Все упоминания данного типа услуги также удалены.", "Успешное выполнение команды", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dataAdapter = new SqlDataAdapter("Select  Название, Описание, Стоимость as 'Стоимость/ед', код from Тип_услуги", dataBaseConnection);
                        dataAdapter.Fill(ds, "Тип_услуги");
                        ShowData.DataSource = ds.Tables["Тип_услуги"];
                        count = ds.Tables["Тип_услуги"].Rows.Count;
                        if (count == 0)
                        {
                            DeleteRecord.Visible = false;
                        }
                        else
                        {
                            DeleteRecord.Visible = true;
                        }
                        ShowData.Update();
                        this.Invalidate();
                        ShowData.Columns[3].Visible = false;
                        break;
                    case "Типы_операций":
                        query = "";
                        query += "Delete from Консультации where код_типа_операции = " + selectedCode + ";";
                        query += "Delete from Типы_операций where код = " + selectedCode;
                        time = DateTime.Today;
                        cmd1 = new SqlCommand("Insert into LogChanges values ('" + UserName + "', 'Удалил запись о типе опрации " + ShowData["Название", ShowData.CurrentCell.RowIndex].Value.ToString() + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                        dataBaseConnection.Open();
                        cmd1.ExecuteNonQuery();
                        dataBaseConnection.Close();
                        ExecuteQuery(query);
                        MessageBox.Show("Удаление выполнено. Все упоминания данного типа операции также удалены.", "Успешное выполнение команды", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dataAdapter = new SqlDataAdapter("Select  тип Название, Описание, код from Типы_операций", dataBaseConnection);
                        dataAdapter.Fill(ds, "Типы_операций");
                        ShowData.DataSource = ds.Tables["Типы_операций"];
                        count = ds.Tables["Типы_операций"].Rows.Count;
                        if (count == 0)
                        {
                            DeleteRecord.Visible = false;
                        }
                        else
                        {
                            DeleteRecord.Visible = true;
                        }
                        ShowData.Update();
                        this.Invalidate();
                        ShowData.Columns[2].Visible = false;
                        break;
                    case "Абоненты":
                        dataAdapter = new SqlDataAdapter("Select Номер, Подключивший, Владелец, Тариф, дата_подключения as 'Дата подключения', дата_отключения as 'Дата_отключения' from Абоненты", dataBaseConnection);
                        dataAdapter.Fill(ds, "Абоненты");
                        ShowData.DataSource = ds.Tables["Абоненты"];
                        ShowData.Update();
                        this.Invalidate();
                        break;
                }
                dataBaseConnection.Close();
            }
            else
                return;
            
        }

        #endregion

        #region Таблицы, Поиск
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
                Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Width = 845;
            this.Height = 549;
            label1.Visible = true;
            label2.Visible = true;
            button1.Visible = false;
            ShowData.Visible = false;
            DeleteRecord.Visible = false;
            Info.Visible = false;
        }

        private void поискToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Search search = new Search(dataBaseConnection);
            search.ShowDialog();
        }

        private void дополнительноToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Procedures Form2 = new Procedures(dataBaseConnection);
            Form2.ShowDialog();
        }

        private void регистрацияНовогоАбонентаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Rights == 5 || Rights == 1)
            {
                NewAbonent newAbonent = new NewAbonent(dataBaseConnection, UserName);
                newAbonent.ShowDialog();
            }
            else
            {
                AccessError();
                return;
            }
        }

        private void завершениеОбслуживанияНомераToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Rights == 5 || Rights == 1)
            {
                ClosingNumber cn = new ClosingNumber(dataBaseConnection, UserName);
                cn.ShowDialog();
            }
            else
            {
                AccessError();
                return;
            }
        }

        private void новаяПродажаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Rights == 5 || Rights == 2)
            {
                Selling sell = new Selling(dataBaseConnection, UserName);
                sell.ShowDialog();
            }
            else
            {
                AccessError();
                return;
            }
        }

        private void новыйСотрудникToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Rights == 5 || Rights == 3)
            {
                Emlpoyees empl = new Emlpoyees(dataBaseConnection, UserName);
                empl.ShowDialog();
            }
            else
            {
                AccessError();
                return;
            }
        }

        private void открытиеНовогоТарифаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Rights == 5 || Rights == 3)
            {
                Tariffs tform = new Tariffs(dataBaseConnection, UserName);
                tform.ShowDialog();
            }
            else
            {
                AccessError();
                return;
            }
        }        
        #endregion

        #region Функции
        private void SetVisiblesForTables(int count, String title)
        {
            label1.Visible = false;
            label2.Visible = false;
            ShowData.Visible = true;
            ShowData.AutoSize = true;      //автоматический размер у грида
            Info.Text = title;
            Info.Visible = true;
            ShowData.Update();
            currentTable = "Клиенты";
            button1.Visible = true;
            this.Invalidate();          //перерисовка формы

        }

        private void ExecuteQuery(String query)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = dataBaseConnection;
            cmd.CommandText = query;
            dataBaseConnection.Open();     //открыли коннект для запроса
            cmd.ExecuteNonQuery();         //выполнили запрос
            dataBaseConnection.Close();    //закрыли коннект для запроса
        }

        private void AccessError()
        {
            MessageBox.Show("Вы не обладаете необходимыми правами для входа в этот раздел.", "Ошибка доступа", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        #endregion

        private void MainForm_Load(object sender, EventArgs e)
        {
            switch (Rights)
            {
                case 0:
                    label4.Text = "Гостевой (только просмотр)";
                    break;
                case 1:
                    label4.Text = "Сотрудник отдела работы с номерами";
                    break;
                case 2:
                    label4.Text = "Сотрудник отдела продаж";
                    break;
                case 3:
                    label4.Text = "Сотрудник отдела кадров";
                    break;
                case 4:
                    label4.Text = "Сотрудник отдела контроля тарифов";
                    break;
                case 5:
                    label4.Text = "Администратор";
                    admPanel.Visible = true;
                    break;
            }
        }

        private void регистрацияКонсультацииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Rights == 5 || Rights == 1)
            {
                Consultations cns = new Consultations(dataBaseConnection, UserName);
                cns.ShowDialog();
            }
            else
            {
                AccessError();
                return;
            }
        }


       
    }
}
