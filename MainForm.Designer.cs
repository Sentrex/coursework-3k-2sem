﻿namespace Programm
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ShowData = new System.Windows.Forms.DataGridView();
            this.DeleteRecord = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.правкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.абонентыToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.регистрацияНовогоАбонентаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.завершениеОбслуживанияНомераToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.регистрацияКонсультацииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.продажиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.новаяПродажаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сотрудникиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.новыйСотрудникToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тарифыToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.открытиеНовогоТарифаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.логИзмененияБазыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.программаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.клиентыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.консультацииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.номераToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.полученныеуслугиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.пополнениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.продажаТелефоновToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ремонтToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сотрудникиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тарифыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.типыУслугToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.типыОперацийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.абонентыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.дополнительноToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поискToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.admPanel = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.Info = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ShowData)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ShowData
            // 
            this.ShowData.AllowUserToAddRows = false;
            this.ShowData.AllowUserToDeleteRows = false;
            this.ShowData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.ShowData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ShowData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ShowData.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ShowData.Location = new System.Drawing.Point(37, 79);
            this.ShowData.Name = "ShowData";
            this.ShowData.ReadOnly = true;
            this.ShowData.Size = new System.Drawing.Size(543, 306);
            this.ShowData.TabIndex = 15;
            this.ShowData.Visible = false;
            // 
            // DeleteRecord
            // 
            this.DeleteRecord.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.DeleteRecord.Location = new System.Drawing.Point(122, 391);
            this.DeleteRecord.Name = "DeleteRecord";
            this.DeleteRecord.Size = new System.Drawing.Size(128, 31);
            this.DeleteRecord.TabIndex = 13;
            this.DeleteRecord.Text = "Удалить запись";
            this.DeleteRecord.UseVisualStyleBackColor = true;
            this.DeleteRecord.Visible = false;
            this.DeleteRecord.Click += new System.EventHandler(this.DeleteRecord_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.правкаToolStripMenuItem,
            this.отчетыToolStripMenuItem,
            this.программаToolStripMenuItem,
            this.toolStripMenuItem1,
            this.поискToolStripMenuItem,
            this.admPanel,
            this.выходToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(829, 24);
            this.menuStrip1.TabIndex = 17;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // правкаToolStripMenuItem
            // 
            this.правкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.абонентыToolStripMenuItem1,
            this.продажиToolStripMenuItem,
            this.сотрудникиToolStripMenuItem1,
            this.тарифыToolStripMenuItem1});
            this.правкаToolStripMenuItem.Name = "правкаToolStripMenuItem";
            this.правкаToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.правкаToolStripMenuItem.Text = "Отделы";
            // 
            // абонентыToolStripMenuItem1
            // 
            this.абонентыToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.регистрацияНовогоАбонентаToolStripMenuItem,
            this.завершениеОбслуживанияНомераToolStripMenuItem,
            this.регистрацияКонсультацииToolStripMenuItem});
            this.абонентыToolStripMenuItem1.Name = "абонентыToolStripMenuItem1";
            this.абонентыToolStripMenuItem1.Size = new System.Drawing.Size(185, 22);
            this.абонентыToolStripMenuItem1.Text = "Работы с номерами";
            // 
            // регистрацияНовогоАбонентаToolStripMenuItem
            // 
            this.регистрацияНовогоАбонентаToolStripMenuItem.Name = "регистрацияНовогоАбонентаToolStripMenuItem";
            this.регистрацияНовогоАбонентаToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.регистрацияНовогоАбонентаToolStripMenuItem.Text = "Регистрация нового номера";
            this.регистрацияНовогоАбонентаToolStripMenuItem.Click += new System.EventHandler(this.регистрацияНовогоАбонентаToolStripMenuItem_Click);
            // 
            // завершениеОбслуживанияНомераToolStripMenuItem
            // 
            this.завершениеОбслуживанияНомераToolStripMenuItem.Name = "завершениеОбслуживанияНомераToolStripMenuItem";
            this.завершениеОбслуживанияНомераToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.завершениеОбслуживанияНомераToolStripMenuItem.Text = "Завершение обслуживания номера";
            this.завершениеОбслуживанияНомераToolStripMenuItem.Click += new System.EventHandler(this.завершениеОбслуживанияНомераToolStripMenuItem_Click);
            // 
            // регистрацияКонсультацииToolStripMenuItem
            // 
            this.регистрацияКонсультацииToolStripMenuItem.Name = "регистрацияКонсультацииToolStripMenuItem";
            this.регистрацияКонсультацииToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.регистрацияКонсультацииToolStripMenuItem.Text = "Регистрация консультации";
            this.регистрацияКонсультацииToolStripMenuItem.Click += new System.EventHandler(this.регистрацияКонсультацииToolStripMenuItem_Click);
            // 
            // продажиToolStripMenuItem
            // 
            this.продажиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новаяПродажаToolStripMenuItem});
            this.продажиToolStripMenuItem.Name = "продажиToolStripMenuItem";
            this.продажиToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.продажиToolStripMenuItem.Text = "Продаж";
            // 
            // новаяПродажаToolStripMenuItem
            // 
            this.новаяПродажаToolStripMenuItem.Name = "новаяПродажаToolStripMenuItem";
            this.новаяПродажаToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.новаяПродажаToolStripMenuItem.Text = "Новая продажа";
            this.новаяПродажаToolStripMenuItem.Click += new System.EventHandler(this.новаяПродажаToolStripMenuItem_Click);
            // 
            // сотрудникиToolStripMenuItem1
            // 
            this.сотрудникиToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новыйСотрудникToolStripMenuItem});
            this.сотрудникиToolStripMenuItem1.Name = "сотрудникиToolStripMenuItem1";
            this.сотрудникиToolStripMenuItem1.Size = new System.Drawing.Size(185, 22);
            this.сотрудникиToolStripMenuItem1.Text = "Кадров";
            // 
            // новыйСотрудникToolStripMenuItem
            // 
            this.новыйСотрудникToolStripMenuItem.Name = "новыйСотрудникToolStripMenuItem";
            this.новыйСотрудникToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.новыйСотрудникToolStripMenuItem.Text = "Управление сотрудниками";
            this.новыйСотрудникToolStripMenuItem.Click += new System.EventHandler(this.новыйСотрудникToolStripMenuItem_Click);
            // 
            // тарифыToolStripMenuItem1
            // 
            this.тарифыToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытиеНовогоТарифаToolStripMenuItem});
            this.тарифыToolStripMenuItem1.Name = "тарифыToolStripMenuItem1";
            this.тарифыToolStripMenuItem1.Size = new System.Drawing.Size(185, 22);
            this.тарифыToolStripMenuItem1.Text = "Контроля тарифов";
            // 
            // открытиеНовогоТарифаToolStripMenuItem
            // 
            this.открытиеНовогоТарифаToolStripMenuItem.Name = "открытиеНовогоТарифаToolStripMenuItem";
            this.открытиеНовогоТарифаToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.открытиеНовогоТарифаToolStripMenuItem.Text = "Управление тарифами";
            this.открытиеНовогоТарифаToolStripMenuItem.Click += new System.EventHandler(this.открытиеНовогоТарифаToolStripMenuItem_Click);
            // 
            // отчетыToolStripMenuItem
            // 
            this.отчетыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.логИзмененияБазыToolStripMenuItem});
            this.отчетыToolStripMenuItem.Name = "отчетыToolStripMenuItem";
            this.отчетыToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.отчетыToolStripMenuItem.Text = "Отчеты";
            // 
            // логИзмененияБазыToolStripMenuItem
            // 
            this.логИзмененияБазыToolStripMenuItem.Name = "логИзмененияБазыToolStripMenuItem";
            this.логИзмененияБазыToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.логИзмененияБазыToolStripMenuItem.Text = "Лог изменения базы";
            this.логИзмененияБазыToolStripMenuItem.Click += new System.EventHandler(this.логИзмененияБазыToolStripMenuItem_Click);
            // 
            // программаToolStripMenuItem
            // 
            this.программаToolStripMenuItem.Name = "программаToolStripMenuItem";
            this.программаToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.программаToolStripMenuItem.Text = "О программе";
            this.программаToolStripMenuItem.Click += new System.EventHandler(this.программаToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.клиентыToolStripMenuItem,
            this.консультацииToolStripMenuItem,
            this.номераToolStripMenuItem,
            this.полученныеуслугиToolStripMenuItem,
            this.пополнениеToolStripMenuItem,
            this.продажаТелефоновToolStripMenuItem,
            this.ремонтToolStripMenuItem,
            this.сотрудникиToolStripMenuItem,
            this.тарифыToolStripMenuItem,
            this.типыУслугToolStripMenuItem,
            this.типыОперацийToolStripMenuItem,
            this.абонентыToolStripMenuItem,
            this.дополнительноToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(155, 20);
            this.toolStripMenuItem1.Text = "Просмотр информации ";
            // 
            // клиентыToolStripMenuItem
            // 
            this.клиентыToolStripMenuItem.Name = "клиентыToolStripMenuItem";
            this.клиентыToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.клиентыToolStripMenuItem.Text = "О клиентах";
            this.клиентыToolStripMenuItem.Click += new System.EventHandler(this.клиентыToolStripMenuItem_Click);
            // 
            // консультацииToolStripMenuItem
            // 
            this.консультацииToolStripMenuItem.Name = "консультацииToolStripMenuItem";
            this.консультацииToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.консультацииToolStripMenuItem.Text = "О проведенных консультациях";
            this.консультацииToolStripMenuItem.Click += new System.EventHandler(this.консультацииToolStripMenuItem_Click);
            // 
            // номераToolStripMenuItem
            // 
            this.номераToolStripMenuItem.Name = "номераToolStripMenuItem";
            this.номераToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.номераToolStripMenuItem.Text = "О номерах";
            this.номераToolStripMenuItem.Click += new System.EventHandler(this.номераToolStripMenuItem_Click);
            // 
            // полученныеуслугиToolStripMenuItem
            // 
            this.полученныеуслугиToolStripMenuItem.Name = "полученныеуслугиToolStripMenuItem";
            this.полученныеуслугиToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.полученныеуслугиToolStripMenuItem.Text = "О предоставленных услугах";
            this.полученныеуслугиToolStripMenuItem.Click += new System.EventHandler(this.полученныеуслугиToolStripMenuItem_Click);
            // 
            // пополнениеToolStripMenuItem
            // 
            this.пополнениеToolStripMenuItem.Name = "пополнениеToolStripMenuItem";
            this.пополнениеToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.пополнениеToolStripMenuItem.Text = "О пополнении счетов";
            this.пополнениеToolStripMenuItem.Click += new System.EventHandler(this.пополнениеToolStripMenuItem_Click);
            // 
            // продажаТелефоновToolStripMenuItem
            // 
            this.продажаТелефоновToolStripMenuItem.Name = "продажаТелефоновToolStripMenuItem";
            this.продажаТелефоновToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.продажаТелефоновToolStripMenuItem.Text = "О продажах телефонов";
            this.продажаТелефоновToolStripMenuItem.Click += new System.EventHandler(this.продажаТелефоновToolStripMenuItem_Click);
            // 
            // ремонтToolStripMenuItem
            // 
            this.ремонтToolStripMenuItem.Name = "ремонтToolStripMenuItem";
            this.ремонтToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.ремонтToolStripMenuItem.Text = "О ремонте телефонов";
            this.ремонтToolStripMenuItem.Click += new System.EventHandler(this.ремонтToolStripMenuItem_Click);
            // 
            // сотрудникиToolStripMenuItem
            // 
            this.сотрудникиToolStripMenuItem.Name = "сотрудникиToolStripMenuItem";
            this.сотрудникиToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.сотрудникиToolStripMenuItem.Text = "О сотрудники компании";
            this.сотрудникиToolStripMenuItem.Click += new System.EventHandler(this.сотрудникиToolStripMenuItem_Click);
            // 
            // тарифыToolStripMenuItem
            // 
            this.тарифыToolStripMenuItem.Name = "тарифыToolStripMenuItem";
            this.тарифыToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.тарифыToolStripMenuItem.Text = "О текущих тарифах";
            this.тарифыToolStripMenuItem.Click += new System.EventHandler(this.тарифыToolStripMenuItem_Click);
            // 
            // типыУслугToolStripMenuItem
            // 
            this.типыУслугToolStripMenuItem.Name = "типыУслугToolStripMenuItem";
            this.типыУслугToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.типыУслугToolStripMenuItem.Text = "О типах предоставляемых услуг";
            this.типыУслугToolStripMenuItem.Click += new System.EventHandler(this.типыУслугToolStripMenuItem_Click);
            // 
            // типыОперацийToolStripMenuItem
            // 
            this.типыОперацийToolStripMenuItem.Name = "типыОперацийToolStripMenuItem";
            this.типыОперацийToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.типыОперацийToolStripMenuItem.Text = "О типах проводимых операций";
            this.типыОперацийToolStripMenuItem.Click += new System.EventHandler(this.типыОперацийToolStripMenuItem_Click);
            // 
            // абонентыToolStripMenuItem
            // 
            this.абонентыToolStripMenuItem.Name = "абонентыToolStripMenuItem";
            this.абонентыToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.абонентыToolStripMenuItem.Text = "Об абонентах сети";
            this.абонентыToolStripMenuItem.Click += new System.EventHandler(this.абонентыToolStripMenuItem_Click_1);
            // 
            // дополнительноToolStripMenuItem
            // 
            this.дополнительноToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.дополнительноToolStripMenuItem.Name = "дополнительноToolStripMenuItem";
            this.дополнительноToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.дополнительноToolStripMenuItem.Text = "Дополнительно";
            this.дополнительноToolStripMenuItem.Click += new System.EventHandler(this.дополнительноToolStripMenuItem_Click);
            // 
            // поискToolStripMenuItem
            // 
            this.поискToolStripMenuItem.Name = "поискToolStripMenuItem";
            this.поискToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.поискToolStripMenuItem.Text = "Поиск";
            this.поискToolStripMenuItem.Click += new System.EventHandler(this.поискToolStripMenuItem_Click);
            // 
            // admPanel
            // 
            this.admPanel.Name = "admPanel";
            this.admPanel.Size = new System.Drawing.Size(154, 20);
            this.admPanel.Text = "Панель администратора";
            this.admPanel.Visible = false;
            this.admPanel.Click += new System.EventHandler(this.admPanel_Click);
            // 
            // выходToolStripMenuItem1
            // 
            this.выходToolStripMenuItem1.Name = "выходToolStripMenuItem1";
            this.выходToolStripMenuItem1.Size = new System.Drawing.Size(53, 20);
            this.выходToolStripMenuItem1.Text = "Выход";
            this.выходToolStripMenuItem1.Click += new System.EventHandler(this.выходToolStripMenuItem1_Click);
            // 
            // Info
            // 
            this.Info.AutoSize = true;
            this.Info.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Info.Location = new System.Drawing.Point(166, 38);
            this.Info.Name = "Info";
            this.Info.Size = new System.Drawing.Size(64, 25);
            this.Info.TabIndex = 18;
            this.Info.Text = "label1";
            this.Info.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("a_AvanteDrp", 50.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(307, 188);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(215, 78);
            this.label1.TabIndex = 19;
            this.label1.Text = "Mobi";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(320, 391);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 31);
            this.button1.TabIndex = 20;
            this.button1.Text = "Скрыть информацию";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("AdverGothic Ho", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Coral;
            this.label2.Location = new System.Drawing.Point(328, 257);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 19);
            this.label2.TabIndex = 21;
            this.label2.Text = "mobile operator";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("a_ConceptoTitulNrFy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(23, 454);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(276, 18);
            this.label3.TabIndex = 22;
            this.label3.Text = "Тип аккаунта пользователя:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("a_CooperBlackRg", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(305, 452);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 19);
            this.label4.TabIndex = 23;
            this.label4.Text = "label4";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 481);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Info);
            this.Controls.Add(this.ShowData);
            this.Controls.Add(this.DeleteRecord);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Оператор мобильной связи \"Mobi\"";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ShowData)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView ShowData;
        private System.Windows.Forms.Button DeleteRecord;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem клиентыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem консультацииToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem номераToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem полученныеуслугиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пополнениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem продажаТелефоновToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ремонтToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сотрудникиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тарифыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem типыУслугToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem типыОперацийToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem программаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem1;
        private System.Windows.Forms.Label Info;
        private System.Windows.Forms.ToolStripMenuItem абонентыToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem поискToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem отчетыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem логИзмененияБазыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem правкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem дополнительноToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem абонентыToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem регистрацияНовогоАбонентаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem завершениеОбслуживанияНомераToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem продажиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem новаяПродажаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сотрудникиToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem новыйСотрудникToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тарифыToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem открытиеНовогоТарифаToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripMenuItem admPanel;
        private System.Windows.Forms.ToolStripMenuItem регистрацияКонсультацииToolStripMenuItem;
    }
}

