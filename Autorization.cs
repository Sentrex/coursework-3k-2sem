﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Programm
{
    public partial class Autorization : Form
    {
        SqlConnection dataBaseConnection;
        public Autorization(SqlConnection sql)
        {
            InitializeComponent();
            dataBaseConnection = sql;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Autorization_FormClosing(object sender, FormClosingEventArgs e)
        {
                Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String login = textBox1.Text;
            if (login == "")
            {
                MessageBox.Show("Введите логин.", "Логин не введен", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            String pass = textBox2.Text;
            if (pass == "")
            {
                MessageBox.Show("Введите пароль.", "Пароль не введен", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            SqlCommand cmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(); 
            cmd.CommandText = @"Select * from [Users] where userName ='" + login + "' and pass = '" + pass + "'";
            cmd.Connection = dataBaseConnection;
            da.SelectCommand = cmd;
            da.Fill(ds, "Пользователь");
            int count = ds.Tables["Пользователь"].Rows.Count;
            if (count == 1)
            {
                MainForm MainForm = new MainForm(dataBaseConnection);
                
                this.Enabled = false;
                String Name ="";        //создаем переменную для имени
                DataTable dt = new DataTable();  //создаем переменную под таблицу
                dt = ds.Tables["Пользователь"];  // получаем таблицу
                DataRow dr = dt.Rows[0];        //берем из таблицы первую (она же и единственная) строку
                Name =(String) dr["Name"];      //получаем оттуда имя пользователяы

                MessageBox.Show("Здравствуйте, " + Name + "!", "Приветствие", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Visible = false;
                MainForm.Show();
            }
            else
            {
                MessageBox.Show("Неверный логин или пароль. Повторите ввод.", "Ошибка авторизации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox1.Clear();
                textBox2.Clear();
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.groupBox1.Visible = true;
            button5.Focus();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            textBox1.Focus();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            button2.Focus();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            String login = textBox3.Text;
            if (login == "")
            {
                MessageBox.Show("Задайте логин.", "Логин не введен", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            String pass = textBox4.Text;
            if (pass == "")
            {
                MessageBox.Show("Задайте пароль.", "Пароль не введен", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            String Name = textBox5.Text;
            if (Name == "")
            {
                MessageBox.Show("Представьтесь системе", "Имя не введено", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            SqlCommand cmd = new SqlCommand();  //запрос будет тут
            cmd.Connection = dataBaseConnection;  //указываем к какой базе подключаться
            cmd.CommandText = @"Insert into [Users] values ('" + login + "', '" + pass + "', '" + Name + "')";  //создаем текст запроса
            dataBaseConnection.Open();
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (System.Data.SqlClient.SqlException)
            {
                MessageBox.Show("Такой логин уже существует. Введите другой.", "Посторяющийся логин", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                textBox3.Focus();
                textBox3.Clear();
                return;
            }
            groupBox1.Visible = false;
            textBox1.Focus();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            MessageBox.Show("Регистрация успешно завершена! Теперь Вы можете войти под своей новой учетной записью!", "Успешная регистрация", MessageBoxButtons.OK, MessageBoxIcon.Information);
            button2.Focus();
        }
    }
}
