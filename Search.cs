﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace Programm
{
    public partial class Search : Form
    {
        SqlConnection dataBaseConnection;
        int numTable = -1, numColumn = -1, flag = -1;
        //двумерный массив [номер таблицы, номер поля]
        String[][] searchCommands = new String[12][];  //каждой строке соотвутствуют поисковые запросы для каждой таблицы

        public Search(SqlConnection sql)
        {
            InitializeComponent();
            dataBaseConnection = sql;
            InitializeSpace();
            ControlBox = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Выбор таблицы
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            numTable = comboBox1.SelectedIndex;
            panel1.Visible = false;
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    comboBox2.Items.Clear();
                    comboBox2.Items.Add("Код клиента");
                    comboBox2.Items.Add("ФИО");
                    comboBox2.Items.Add("Дата рождения");
                    comboBox2.Items.Add("Дата регистрации");
                    break;
                case 1:
                    comboBox2.Items.Clear();
                    comboBox2.Items.Add("Код консультации");
                    comboBox2.Items.Add("Сотрудник");
                    comboBox2.Items.Add("Клиент");
                    comboBox2.Items.Add("Тип операции");
                    comboBox2.Items.Add("Дата обращения");
                    break;
                case 2:
                    comboBox2.Items.Clear();
                    comboBox2.Items.Add("Код номера");
                    comboBox2.Items.Add("Номер");
                    comboBox2.Items.Add("Сотрудник");
                    comboBox2.Items.Add("Владелец номера");
                    comboBox2.Items.Add("Тариф");
                    comboBox2.Items.Add("Дата подключения");
                    comboBox2.Items.Add("Дата отключения");
                    comboBox2.Items.Add("Среднее пополнение");
                    break;
                case 3:
                    comboBox2.Items.Clear();
                    comboBox2.Items.Add("Код полученной услуги");
                    comboBox2.Items.Add("Тип услуги");
                    comboBox2.Items.Add("Номер");
                    comboBox2.Items.Add("Количество");
                    comboBox2.Items.Add("Дата предоставления");
                    break;
                case 4:
                    comboBox2.Items.Clear();
                    comboBox2.Items.Add("Код пополнения");
                    comboBox2.Items.Add("Номер");
                    comboBox2.Items.Add("Сумма");
                    comboBox2.Items.Add("Дата пополнения");
                    break;
                case 5:
                    comboBox2.Items.Clear();
                    comboBox2.Items.Add("Код чека");
                    comboBox2.Items.Add("Модель телефона");
                    comboBox2.Items.Add("Стоимость");
                    comboBox2.Items.Add("Дата продажи");
                    comboBox2.Items.Add("Продавец");
                    comboBox2.Items.Add("Покупатель");
                    break;
                case 6:
                    comboBox2.Items.Clear();
                    comboBox2.Items.Add("Код ремонта");
                    comboBox2.Items.Add("Клиент");
                    comboBox2.Items.Add("Дата приема");
                    break;
                case 7:
                    comboBox2.Items.Clear();
                    comboBox2.Items.Add("Код сотрудника");
                    comboBox2.Items.Add("ФИО сотрудника");
                    comboBox2.Items.Add("Должность");
                    break;
                case 8:
                    comboBox2.Items.Clear();
                    comboBox2.Items.Add("Код тарифа");
                    comboBox2.Items.Add("Название тарифа");
                    comboBox2.Items.Add("Абонентская плата");
                    break;
                case 9:
                    comboBox2.Items.Clear();
                    comboBox2.Items.Add("Код типа услуги");
                    comboBox2.Items.Add("Название услуги");
                    comboBox2.Items.Add("Описание услуги");
                    comboBox2.Items.Add("Стоимость за единицу");
                    break;
                case 10:
                    comboBox2.Items.Clear();
                    comboBox2.Items.Add("Код типа операции");
                    comboBox2.Items.Add("Тип операции");
                    comboBox2.Items.Add("Описание операции");
                    break;
                case 11:
                    comboBox2.Items.Clear();
                    comboBox2.Items.Add("Код абонента");
                    comboBox2.Items.Add("Номер");
                    comboBox2.Items.Add("Сотрудник");
                    comboBox2.Items.Add("Владелец номера");
                    comboBox2.Items.Add("Название тарифа");
                    comboBox2.Items.Add("Дата подключения");
                    break;
            }
            comboBox2.Visible = true;
            choiseColumn.Visible = true;
            textBox1.Visible = false;
            goButton.Visible = false;
            textBox1.Text = "";
            searchData.Visible = false;
            comboBox2.Text = "";
            dataGridView1.Visible = false;
            dataGridView1.DataSource = null;
        }
        #endregion

        #region Выбор поля
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            searchData.Visible = true;
            goButton.Visible = true;
            dataGridView1.Visible = false;
            dataGridView1.DataSource = null;
            numColumn = comboBox2.SelectedIndex;
            String value = (String)comboBox2.Items[numColumn];
            if (comboBox2.SelectedItem.ToString() == "Сумма" || comboBox2.SelectedItem.ToString() == "Количество" || comboBox2.SelectedItem.ToString() == "Стоимость за единицу" || comboBox2.SelectedItem.ToString() == "Среднее пополнение" || comboBox2.SelectedItem.ToString() == "Абонентская плата" || comboBox2.SelectedItem.ToString() == "Стоимость")
            {
                searchData.Text = "До какого значения вести поиск:";
                textBox1.MaxLength = 10;
            }
            else
            {
                searchData.Text = "Данные для поиска:";
                textBox1.MaxLength = 39;
            }
            textBox1.Clear();
            if (value.StartsWith("Дата")){
                panel1.Visible = true;
                textBox1.Visible = false;
                goButton.Enabled = true;
                flag = 1;       //для даты
            }
            else{
                textBox1.Visible = true;
                panel1.Visible = false;
                goButton.Enabled = false;
                flag = 0;  //для текста
            }
        }
        #endregion

        #region Запуск поиска
        private void goButton_Click(object sender, EventArgs e)
        {
            if (textBox1.Visible)
                SearchInTables(textBox1.Text, 0);
            else
                SearchInTables(dateTimePicker1.Value.ToString("dd-MM-yyyy"), 1);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                goButton.Enabled = false;
            }
            else
            {
                goButton.Enabled = true;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (textBox1.Text == "")
                {
                    MessageBox.Show("Не задано значение для поиска!", "Некорректное условие поиска", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    SearchInTables(textBox1.Text, 0);
                }
                
            }
        }
        #endregion

        #region Поиск
        private void SearchInTables(String search, int flag)
        {
            int code;
            String query;
            if (comboBox2.SelectedIndex == 0 || comboBox2.SelectedItem.ToString() == "Сумма" || comboBox2.SelectedItem.ToString() == "Количество" || comboBox2.SelectedItem.ToString() == "Стоимость за единицу" || comboBox2.SelectedItem.ToString() == "Среднее пополнение" || comboBox2.SelectedItem.ToString() == "Абонентская плата" || comboBox2.SelectedItem.ToString() == "Стоимость")
            {
                if (Int32.TryParse(search, out code))
                {
                    if (code >= Int32.MaxValue)
                    {
                        Errors(3);
                        return;
                    }
                    else
                        query = searchCommands[numTable][numColumn] + code + "')";
                    
                }
                else
                    {
                        Errors(1);
                        return;
                    }
            }
            else
            {
                if (flag == 0)
                    query = searchCommands[numTable][numColumn] + search + "%')";
                else
                    query = searchCommands[numTable][numColumn] + search + "' and '" + dateTimePicker2.Value.ToString("dd-MM-yyyy") + "')";
            }
            DataSet ds = new DataSet();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(query, dataBaseConnection);
            dataAdapter.Fill(ds, "Поиск");
            int count = ds.Tables["Поиск"].Rows.Count;
            if (count == 0)
            {
                dataGridView1.DataSource = null;
                dataGridView1.Visible = false;
                Errors(2);
                return;
            }
            dataGridView1.Visible = true;
            dataGridView1.DataSource = ds.Tables["Поиск"];
            this.Invalidate();
            dataGridView1.Update();
            
            
        }

        private void InitializeSpace()
        {
            //клиенты
            searchCommands[0] = new String[4] {"(SELECT ФИО, дата_рождения as 'Дата рождения', дата_регистрации as 'Дата регистрации' from Клиенты where код = '",
                                               "(SELECT ФИО, дата_рождения as 'Дата рождения', дата_регистрации as 'Дата регистрации' from Клиенты where ФИО like '%",
                                               "(SELECT ФИО, дата_рождения as 'Дата рождения', дата_регистрации as 'Дата регистрации' from Клиенты where дата_рождения between '",
                                               "(SELECT ФИО, дата_рождения as 'Дата рождения', дата_регистрации as 'Дата регистрации' from Клиенты where дата_регистрации between '",
                                                };
            //консультации
            searchCommands[1] = new String[5] {"(Select Сотрудники.ФИО as Сотрудник, Клиенты.ФИО as Клиент, Типы_операций.Тип as Операция, дата_обращения as Дата from Консультации, Сотрудники, Клиенты, Типы_операций where код_сотрудника=Сотрудники.код and код_клиента = Клиенты.код and код_типа_операции = Типы_операций.код and Консультации.код = '",
                                               "Select Сотрудники.ФИО as Сотрудник, Клиенты.ФИО as Клиент, Типы_операций.Тип as Операция, дата_обращения as Дата from Консультации, Сотрудники, Клиенты, Типы_операций where код_сотрудника=Сотрудники.код and код_клиента = Клиенты.код and код_типа_операции = Типы_операций.код and Консультации.код_сотрудника in (select код from Сотрудники where ФИО like '%",
                                               "Select Сотрудники.ФИО as Сотрудник, Клиенты.ФИО as Клиент, Типы_операций.Тип as Операция, дата_обращения as Дата from Консультации, Сотрудники, Клиенты, Типы_операций where код_сотрудника=Сотрудники.код and код_клиента = Клиенты.код and код_типа_операции = Типы_операций.код and Консультации.код_клиента in (select код from Клиенты where ФИО like '%",
                                               "Select Сотрудники.ФИО as Сотрудник, Клиенты.ФИО as Клиент, Типы_операций.Тип as Операция, дата_обращения as Дата from Консультации, Сотрудники, Клиенты, Типы_операций where код_сотрудника=Сотрудники.код and код_клиента = Клиенты.код and код_типа_операции = Типы_операций.код and код_типа_операции = (select код from Типы_операций where тип like '%",
                                               "(Select Сотрудники.ФИО as Сотрудник, Клиенты.ФИО as Клиент, Типы_операций.Тип as Операция, дата_обращения as Дата from Консультации, Сотрудники, Клиенты, Типы_операций where код_сотрудника=Сотрудники.код and код_клиента = Клиенты.код and код_типа_операции = Типы_операций.код and дата_обращения between '"
                                               };
            //номера
            searchCommands[2] = new String[8] {"(Select номер, Сотрудники.ФИО as Подключивший, Клиенты.ФИО as Владелец, dbo.Среднее_пополнение(Номера.код) as 'Среднее пополнение',  Тарифы.Название as Тариф, дата_подключения as 'Дата подключения', дата_отключения as 'Дата отключения' from Номера, Сотрудники, Клиенты, Тарифы where код_сотрудника=Сотрудники.код and код_владельца=Клиенты.код and код_тарифа = Тарифы.код and Номера.код = '",
                                               "(Select номер, Сотрудники.ФИО as Подключивший, Клиенты.ФИО as Владелец, dbo.Среднее_пополнение(Номера.код) as 'Среднее пополнение',  Тарифы.Название as Тариф, дата_подключения as 'Дата подключения', дата_отключения as 'Дата отключения' from Номера, Сотрудники, Клиенты, Тарифы where код_сотрудника=Сотрудники.код and код_владельца=Клиенты.код and код_тарифа = Тарифы.код and номер like '%",
                                               "Select номер, Сотрудники.ФИО as Подключивший, Клиенты.ФИО as Владелец, dbo.Среднее_пополнение(Номера.код) as 'Среднее пополнение',  Тарифы.Название as Тариф, дата_подключения as 'Дата подключения', дата_отключения as 'Дата отключения' from Номера, Сотрудники, Клиенты, Тарифы where код_сотрудника=Сотрудники.код and код_владельца=Клиенты.код and код_тарифа = Тарифы.код and код_сотрудника in (select код from Сотрудники where ФИО like '%",
                                               "Select номер, Сотрудники.ФИО as Подключивший, Клиенты.ФИО as Владелец, dbo.Среднее_пополнение(Номера.код) as 'Среднее пополнение',  Тарифы.Название as Тариф, дата_подключения as 'Дата подключения', дата_отключения as 'Дата отключения' from Номера, Сотрудники, Клиенты, Тарифы where код_сотрудника=Сотрудники.код and код_владельца=Клиенты.код and код_тарифа = Тарифы.код and код_владельца in (select код from Клиенты where ФИО like '%",
                                               "Select номер, Сотрудники.ФИО as Подключивший, Клиенты.ФИО as Владелец, dbo.Среднее_пополнение(Номера.код) as 'Среднее пополнение',  Тарифы.Название as Тариф, дата_подключения as 'Дата подключения', дата_отключения as 'Дата отключения' from Номера, Сотрудники, Клиенты, Тарифы where код_сотрудника=Сотрудники.код and код_владельца=Клиенты.код and код_тарифа = Тарифы.код and код_тарифа = (select код from Тарифы where название like '%",
                                               "(Select номер, Сотрудники.ФИО as Подключивший, Клиенты.ФИО as Владелец, dbo.Среднее_пополнение(Номера.код) as 'Среднее пополнение',  Тарифы.Название as Тариф, дата_подключения as 'Дата подключения', дата_отключения as 'Дата отключения' from Номера, Сотрудники, Клиенты, Тарифы where код_сотрудника=Сотрудники.код and код_владельца=Клиенты.код and код_тарифа = Тарифы.код and дата_подключения between '",
                                               "(Select номер, Сотрудники.ФИО as Подключивший, Клиенты.ФИО as Владелец, dbo.Среднее_пополнение(Номера.код) as 'Среднее пополнение',  Тарифы.Название as Тариф, дата_подключения as 'Дата подключения', дата_отключения as 'Дата отключения' from Номера, Сотрудники, Клиенты, Тарифы where код_сотрудника=Сотрудники.код and код_владельца=Клиенты.код and код_тарифа = Тарифы.код and дата_отключения between '",
                                               "Select номер, Сотрудники.ФИО as Подключивший, Клиенты.ФИО as Владелец, dbo.Среднее_пополнение(Номера.код) as 'Среднее пополнение',  Тарифы.Название as Тариф, дата_подключения as 'Дата подключения', дата_отключения as 'Дата отключения' from Номера, Сотрудники, Клиенты, Тарифы where код_сотрудника=Сотрудники.код and код_владельца=Клиенты.код and код_тарифа = Тарифы.код and код_владельца in (select код from Клиенты where dbo.Среднее_пополнение(Номера.код) <= '"
                                              };
            //полученные_услуги
            searchCommands[3] = new String[5] {"(Select Тип_услуги.название as 'Тип услуги', Номера.номер, количество, дата_предоставления as 'Дата предоставления' from Полученные_услуги, Номера, Тип_услуги where Номера.код = код_номера and код_типа_услуги = Тип_услуги.код and Полученные_услуги.код = '",
                                               "Select Тип_услуги.название as 'Тип услуги', Номера.номер, количество, дата_предоставления as 'Дата предоставления' from Полученные_услуги, Номера, Тип_услуги where Номера.код = код_номера and код_типа_услуги = Тип_услуги.код and код_типа_услуги = (select код from Тип_услуги where название like '%",
                                               "Select Тип_услуги.название as 'Тип услуги', Номера.номер, количество, дата_предоставления as 'Дата предоставления' from Полученные_услуги, Номера, Тип_услуги where Номера.код = код_номера and код_типа_услуги = Тип_услуги.код and код_номера = (select код from Номера where номер like '%",
                                               "(Select Тип_услуги.название as 'Тип услуги', Номера.номер, количество, дата_предоставления as 'Дата предоставления' from Полученные_услуги, Номера, Тип_услуги where Номера.код = код_номера and код_типа_услуги = Тип_услуги.код and количество <= '",
                                               "(Select Тип_услуги.название as 'Тип услуги', Номера.номер, количество, дата_предоставления as 'Дата предоставления' from Полученные_услуги, Номера, Тип_услуги where Номера.код = код_номера and код_типа_услуги = Тип_услуги.код and дата_предоставления between '"
                                              };
            //пополнение
            searchCommands[4] = new String[4] {"(Select Номера.номер, сумма, дата_пополнения as 'Дата пополнения' from Пополнение, Номера where код_номера=Номера.код and Пополнение.код = '",
                                               "Select Номера.номер, сумма, дата_пополнения as 'Дата пополнения' from Пополнение, Номера where Номера.код = код_номера and код_номера = (select код from Номера where номер like '%",
                                               "(Select Номера.номер, сумма, дата_пополнения as 'Дата пополнения' from Пополнение, Номера where код_номера=Номера.код and сумма <= '",
                                               "(Select Номера.номер, сумма, дата_пополнения as 'Дата пополнения' from Пополнение, Номера where код_номера=Номера.код and дата_пополнения between '"
                                              };
            //продажа_телефонов
            searchCommands[5] = new String[6] {"(Select Продажа_телефонов.код 'Номер чека', модель, стоимость, дата_продажи 'Дата продажи', Сотрудники.ФИО as Продавец, Клиенты.ФИО as Покупатель from Продажа_телефонов, Сотрудники, Клиенты where код_сотрудника = Сотрудники.код and код_покупателя = Клиенты.код and Продажа_телефонов.код = '",
                                               "(Select Продажа_телефонов.код 'Номер чека', модель, стоимость, дата_продажи 'Дата продажи', Сотрудники.ФИО as Продавец, Клиенты.ФИО as Покупатель from Продажа_телефонов, Сотрудники, Клиенты where код_сотрудника = Сотрудники.код and код_покупателя = Клиенты.код and модель like '%",
                                               "(Select Продажа_телефонов.код 'Номер чека', модель, стоимость, дата_продажи 'Дата продажи', Сотрудники.ФИО as Продавец, Клиенты.ФИО as Покупатель from Продажа_телефонов, Сотрудники, Клиенты where код_сотрудника = Сотрудники.код and код_покупателя = Клиенты.код and стоимость <= '",
                                               "(Select Продажа_телефонов.код 'Номер чека', модель, стоимость, дата_продажи 'Дата продажи', Сотрудники.ФИО as Продавец, Клиенты.ФИО as Покупатель from Продажа_телефонов, Сотрудники, Клиенты where код_сотрудника = Сотрудники.код and код_покупателя = Клиенты.код and дата_продажи between '",
                                               "Select Продажа_телефонов.код 'Номер чека', модель, стоимость, дата_продажи 'Дата продажи', Сотрудники.ФИО as Продавец, Клиенты.ФИО as Покупатель from Продажа_телефонов, Сотрудники, Клиенты where код_сотрудника = Сотрудники.код and код_покупателя = Клиенты.код and код_сотрудника in (Select код from Сотрудники where ФИО like '%",
                                               "Select Продажа_телефонов.код 'Номер чека', модель, стоимость, дата_продажи 'Дата продажи', Сотрудники.ФИО as Продавец, Клиенты.ФИО as Покупатель from Продажа_телефонов, Сотрудники, Клиенты where код_сотрудника = Сотрудники.код and код_покупателя = Клиенты.код and код_покупателя in (select код from Клиенты where ФИО like '%"
                                              };
            //ремонт
            searchCommands[6] = new String[3] {"(Select Клиенты.ФИО as Клиент, дата_приема as 'Дата приема' from Ремонт, Клиенты where Клиенты.код = код_клиента and Ремонт.код = '",
                                               "Select Клиенты.ФИО as Клиент, дата_приема as 'Дата приема' from Ремонт, Клиенты where Клиенты.код = код_клиента and код_клиента = (select код from Клиенты where ФИО like '%",
                                               "(Select Клиенты.ФИО as Клиент, дата_приема as 'Дата приема' from Ремонт, Клиенты where Клиенты.код = код_клиента and дата_приема between '"
                                              };
            //сотрудники
            searchCommands[7] = new String[3] {"(Select ФИО, Должность from Сотрудники where код = '",
                                              "(Select ФИО, Должность from Сотрудники where ФИО like '%",
                                              "(Select ФИО, Должность from Сотрудники where должность like '%"
                                              };
            //тарифы
            searchCommands[8] = new String[3] {"(Select Название, Абонплата from Тарифы where код = '",
                                               "(Select Название, Абонплата from Тарифы where название like '%",
                                               "(Select Название, Абонплата from Тарифы where Абонплата <= '"
                                              };
            //типы_услуг
            searchCommands[9] = new String[4] {"(Select Название, Описание, Стоимость as 'Стоимость/ед' from Тип_услуги where код = '",
                                               "(Select Название, Описание, Стоимость as 'Стоимость/ед' from Тип_услуги where название like '%",
                                               "(Select Название, Описание, Стоимость as 'Стоимость/ед' from Тип_услуги where описание like '%",
                                               "(Select Название, Описание, Стоимость as 'Стоимость/ед' from Тип_услуги where Стоимость <= '"
                                              };
            //типы_операций
            searchCommands[10] = new String[3] {"(Select тип Название, Описание from Типы_операций where код = '",
                                                "(Select тип Название, Описание from Типы_операций where тип like '%",
                                                "(Select тип Название, Описание from Типы_операций where описание like '%"
                                              };
            //абоненты
            searchCommands[11] = new String[6] {"(Select номер, Владелец, тариф where дата_отключения is not null and код = '",
                                                "(Select номер, Владелец, тариф where дата_отключения is not null and номер like '%",
                                                "(Select номер, Владелец, тариф where дата_отключения is not null and подключивший like '%",
                                                "(Select номер, Владелец, тариф where дата_отключения is not null and владелец like '%",
                                                "(Select номер, Владелец, тариф where дата_отключения is not null and тариф like '%",
                                                "(Select номер, Владелец, тариф where дата_отключения is not null and дата_подключения between '"
                                              };
        }
        #endregion

        #region Ошибки
        private void Errors(int codeError)
        {
            switch (codeError)
            {
                case 1:
                    MessageBox.Show("Для поиска по цифровому полю введено текстовое значение.", "Ошибка ввода данных", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox1.Clear();
                    textBox1.Focus();
                    break;
                case 2:
                    MessageBox.Show("По Вашему запросу записи не найдены. Попробуйте изменить данные поиска.", "Информация о поиске", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBox1.Focus();
                    textBox1.SelectionLength = textBox1.Text.Length;
                    break;
                case 3:
                    MessageBox.Show("Превышено максимальное значение типа данных. (" + Int32.MaxValue + ")", "Превышено максимальное значение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox1.Clear();
                    textBox1.Focus();
                    break;
            }
            
        }
        #endregion

        private void dateTimePicker1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                SearchInTables(dateTimePicker1.Value.ToString("dd-MM-yyyy"), 1);
            }
        }

        private void dateTimePicker2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                SearchInTables(dateTimePicker1.Value.ToString("dd-MM-yyyy"), 1);
            }
        }
    }
}
