﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Programm
{
    public partial class Authorization : Form
    {
        SqlConnection dataBaseConnection;
        int RightsLevel = 0;
        public Authorization(SqlConnection sql)
        {
            InitializeComponent();
            dataBaseConnection = sql;
            button2.Focus();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Autorization_FormClosing(object sender, FormClosingEventArgs e)
        {
                Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Entering();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.groupBox1.Visible = true;
            textBox3.Focus();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            textBox1.Focus();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            button2.Focus();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Registration();
        }



        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                Entering();
            }
            else
                return;
        }

        private void Entering()
        {
            String login = textBox1.Text;
            if (login == "")
            {
                MessageBox.Show("Введите логин.", "Логин не введен", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox1.Focus();
                textBox2.Clear();
                return;
            }
            String pass = textBox2.Text;
            if (pass == "")
            {
                MessageBox.Show("Введите пароль.", "Пароль не введен", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox2.Focus();
                return;
            }
            SqlCommand cmd = new SqlCommand();      //новый запрос sql
            DataSet ds = new DataSet();             //новый набор данных
            SqlDataAdapter da = new SqlDataAdapter();
            cmd.CommandText = @"Select * from [Users] where userName ='" + login + "' and pass = '" + pass + "'";   //сам запрос
            cmd.Connection = dataBaseConnection;    //коннект для базы
            da.SelectCommand = cmd;     //выполнение запроса
            da.Fill(ds, "Пользователь");    //заполнение
            int count = ds.Tables["Пользователь"].Rows.Count;
            if (count == 1)
            {
                

                this.Enabled = false;
                String Name = "";        //создаем переменную для имени
                DataTable dt = new DataTable();  //создаем переменную под таблицу
                dt = ds.Tables["Пользователь"];  // получаем таблицу
                DataRow dr = dt.Rows[0];        //берем из таблицы первую (она же и единственная) строку
                Name = (String)dr["Name"];      //получаем оттуда имя пользователя
                String userName = (String)dr["userName"];
                MessageBox.Show("Здравствуйте, " + Name + "!", "Приветствие", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DateTime time = DateTime.Today;
                SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + login + "', 'Авторизация', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                dataBaseConnection.Open();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                RightsLevel = (int)dr["Rights"];
                this.Visible = false;
                MainForm MainForm = new MainForm(dataBaseConnection, RightsLevel, userName);
                MainForm.Show();
            }
            else
            {
                MessageBox.Show("Неверный логин или пароль. Повторите ввод.", "Ошибка авторизации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox1.Clear();
                textBox2.Clear();
                textBox1.Focus();
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                Entering();
            }
            else
                return;
        }

        private int setRights()
        {
            String rightpass = textBox6.Text;       //получили пароль регистрации
            DataSet dsr = new DataSet();
            SqlDataAdapter dar = new SqlDataAdapter("select код, пароль from Rights where пароль = '" + rightpass + "'", dataBaseConnection);
            dar.Fill(dsr, "Пароли");
            DataTable dt = dsr.Tables["Пароли"];
            int count = dt.Rows.Count;
            if (count != 0)
            {
                DataRow dr = dt.Rows[0];
                RightsLevel = (int)dr["код"];
                RightsLevel--;
            }
            else { 
                MessageBox.Show("Пароль регистрации введен неверно. Повторите ввод или обратитесь к администратору за актулаьным паролем.", "Пароль регистрации неверен", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox6.Clear();
                textBox6.Focus();
                return -1;
            }
            return 0;
            /*switch (rightpass)
            {
                case "" :
                    RightsLevel = 0;
                    break;
                case "numbers1":
                    RightsLevel = 1;
                    break;
                case "sellingP":
                    RightsLevel = 2;
                    break;
                case "cadresE":
                    RightsLevel = 3;
                    break;
                case "tariffsT":
                    RightsLevel = 4;
                    break;
                case "adminpass77":
                    RightsLevel = 5;
                    break;
            }*/
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
                Registration();
        }

        private void Registration()
        {
            String login = textBox3.Text;
            if (login == "")
            {
                MessageBox.Show("Задайте логин.", "Логин не введен", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            String pass = textBox4.Text;
            if (pass == "")
            {
                MessageBox.Show("Задайте пароль.", "Пароль не введен", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            String Name = textBox5.Text;
            if (Name == "")
            {
                MessageBox.Show("Представьтесь системе.", "Имя не введено", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int fl = setRights();
            if (fl == -1)
                return;
            else
            {
                SqlCommand cmd = new SqlCommand();  //запрос будет тут
                cmd.Connection = dataBaseConnection;  //указываем к какой базе подключаться
                cmd.CommandText = @"Insert into [Users] values ('" + login + "', '" + pass + "', '" + Name + "', " + RightsLevel + ")";  //создаем текст запроса
                dataBaseConnection.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                    DateTime time = DateTime.Today;
                    SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + login + "', 'Зарегистрировался', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                    cmd1.ExecuteNonQuery();
                }
                catch (System.Data.SqlClient.SqlException)
                {
                    MessageBox.Show("Такой логин уже существует. Введите другой.", "Повторяющийся логин", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    textBox3.Focus();
                    textBox3.Clear();
                    return;
                }
                finally
                {
                    dataBaseConnection.Close();
                }
                groupBox1.Visible = false;
                textBox1.Focus();
                textBox3.Clear();
                textBox4.Clear();
                textBox5.Clear();
                MessageBox.Show("Регистрация успешно завершена! Теперь Вы можете войти под своей новой учетной записью!", "Успешная регистрация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                button2.Focus();
            }
        }
    }
}
