﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Programm
{
    public partial class Consultations : Form
    {
        SqlConnection dataBaseConnection;
        DataSet ds;
        SqlDataAdapter clientsAdapter;  //для клиентов
        SqlDataAdapter emplAdapter;     //для сотрудников
        SqlDataAdapter tarAdapter;      //для типоа
        String login;
        public Consultations(SqlConnection sql, String lg)
        {
            login = lg;
            dataBaseConnection = sql;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RegistrationClient.Visible = true;
            regOper.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String fio = FIO.Text;      //получение фио клиента
            String Date = dateTimePicker1.Value.ToString("yyyy-MM-dd");
            DateTime nowDate = DateTime.Today;                  //сегодняшняя дата
            String TimeReg = nowDate.ToString("yyyy-MM-dd");    //конвертирование под формат сервера
            SqlCommand cmd = new SqlCommand("Insert into Клиенты values('" + fio + "', '" + Date + "', '" + TimeReg + "')");
            cmd.Connection = dataBaseConnection;
            dataBaseConnection.Open();
            cmd.ExecuteNonQuery();
            dataBaseConnection.Close();
            DateTime time = DateTime.Today;
            SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + login + "', 'Зарегистрировал нового клиента " + fio + "', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
            dataBaseConnection.Open();
            cmd1.ExecuteNonQuery();
            dataBaseConnection.Close();
            RegistrationClient.Visible = false;
            FIO.Clear();
            RefreshData();
        }

        private void RefreshData()
        {
            ds = new DataSet();
            String search1 = textBox1.Text;
            String search2 = textBox2.Text;
            emplAdapter = new SqlDataAdapter("Select ФИО, Должность, код as 'Код сотрудника' from Сотрудники where ФИО like '" + search1 + "%' order by ФИО", dataBaseConnection);       //получили сотрудников
            emplAdapter.Fill(ds, "Сотрудники");
            clientsAdapter = new SqlDataAdapter("SELECT ФИО, дата_рождения as 'Дата рождения', дата_регистрации as 'Дата регистрации', код 'Код клиента' FROM Клиенты where ФИО like '" + search2 + "%' order by ФИО", dataBaseConnection);       //получили клиентов
            clientsAdapter.Fill(ds, "Клиенты");
            Employees.DataSource = ds.Tables["Сотрудники"]; //отображение сотрудников
            Clients.DataSource = ds.Tables["Клиенты"]; //отображение клиентов
            Clients.Columns[3].Visible = false;
            Employees.Columns[2].Visible = false;
            Employees.Update();
            Clients.Update();
            tarAdapter = new SqlDataAdapter("select Тип, код from Типы_операций order by тип", dataBaseConnection);
            tarAdapter.Fill(ds, "Типы");
            Types.DataSource = ds.Tables["Типы"];
            Types.Columns[1].Visible = false;
            Types.Update();
        }

        private void Consultations_Load(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            RegistrationClient.Visible = false;
            FIO.Clear();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DateTime time = DateTime.Today;
            SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + login + "', 'Зарегистрировал консультацию ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
            dataBaseConnection.Open();
            cmd1.ExecuteNonQuery();
            dataBaseConnection.Close();
            cmd1 = new SqlCommand("Insert into Консультации values ('" + Employees["Код сотрудника", Employees.CurrentCell.RowIndex].Value.ToString() + "', '" + Clients["Код клиента", Clients.CurrentCell.RowIndex].Value.ToString() + "', '" + Types["код", Types.CurrentCell.RowIndex].Value.ToString() + "', '" + DateTime.Today + "')");
            cmd1.Connection = dataBaseConnection;
            dataBaseConnection.Open();
            cmd1.ExecuteNonQuery();
            dataBaseConnection.Close();
            MessageBox.Show("Успешно зарегистрировано.", "Успешное выполнение операции", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (MessageBox.Show("Продолжить регистрацию консультаций?", "Продолжение регистрации консультаций", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                RefreshData();
            }
            else
                this.Close();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox3.Clear();
            textBox4.Clear();
            regOper.Visible = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            String name = textBox3.Text;
            String op = textBox4.Text;
            SqlCommand cmd = new SqlCommand("Insert into Типы_операций values ('" + name + "', '" + op + "')", dataBaseConnection);
            dataBaseConnection.Open();
            cmd.ExecuteNonQuery();
            dataBaseConnection.Close();
            textBox3.Clear();
            textBox4.Clear();
            regOper.Visible = false;
            RefreshData();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            regOper.Visible = true;
            RegistrationClient.Visible = false;
        }

    }
}
