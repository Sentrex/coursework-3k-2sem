﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Programm
{
    public partial class AdminPanel : Form
    {
        SqlConnection dataBaseConnection;
        DataSet ds;
        int AccLevel;
        String login1;
        public AdminPanel(SqlConnection sql, String lg)
        {
            login1 = lg;
            dataBaseConnection = sql;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AdminPanel_Load(object sender, EventArgs e)
        {
            RefreshData(1);
            ControlBox = false;
        }


        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                if (MessageBox.Show("Убрать пароль для типа аккаунта?", "Подтверждение действия", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ChangePass();
                    groupBox1.Visible = false;
                }
                else
                    return;
            }
            else
            {
                ChangePass();
                groupBox1.Visible = false;
            }
        }

        private void ChangePass()
        {
            String pass = textBox1.Text;
            SqlCommand cmd = new SqlCommand("Update Rights set пароль = '" + pass + "' where код = " + dataGridView1["код", dataGridView1.CurrentCell.RowIndex].Value, dataBaseConnection);
            dataBaseConnection.Open();
            cmd.ExecuteNonQuery();
            dataBaseConnection.Close();
            RefreshData(1);
            textBox1.Clear();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            textBox1.Clear();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            RefreshData(1);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            groupBox2.Visible = false;
            textBox2.Clear();
        }

        private void RefreshData(int numGr)
        {
            ds = new DataSet();
            SqlDataAdapter da;
            if (numGr == 1)
            {
                da = new SqlDataAdapter("Select тип_аккаунта as 'Тип аккаунта', пароль, код from Rights", dataBaseConnection);
                da.Fill(ds, "Пароли");
                dataGridView1.DataSource = ds.Tables["Пароли"];
                dataGridView1.Columns[2].Visible = false;
                dataGridView1.Update();
            }
            else
            {
                da = new SqlDataAdapter("Select userName as 'login', pass as 'password', Name as 'Имя', Rights as 'rights' from Users where userName like '" + textBox2.Text + "%'", dataBaseConnection);
                da.Fill(ds, "Доступ");
                dataGridView2.DataSource = ds.Tables["Доступ"];
                dataGridView2.Columns[3].Visible = false;
                dataGridView2.Update();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            RefreshData(2);
            groupBox2.Visible = true;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            RefreshData(2);
            setCheck();
        }

        private void setCheck()
        {
            int level;
            level = (int)dataGridView2["rights", dataGridView2.CurrentCell.RowIndex].Value;
            switch (level)
            {
                case 0:
                    radioButton1.Checked = true;
                    break;
                case 1:
                    radioButton2.Checked = true;
                    break;
                case 2:
                    radioButton3.Checked = true;
                    break;
                case 3:
                    radioButton4.Checked = true;
                    break;
                case 4:
                    radioButton5.Checked = true;
                    break;
                case 5:
                    radioButton6.Checked = true;
                    break;
            }
        }

        private void dataGridView2_VisibleChanged(object sender, EventArgs e)
        {
            setCheck();
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            setCheck();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Подтверждаете изменение уровня доступа учетной записи?", "Подтверждение действия", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                String login = (String)dataGridView2["login", dataGridView2.CurrentCell.RowIndex].Value;
                SqlCommand cmd = new SqlCommand("Update Users set Rights = " + AccLevel + " where userName = '" + login + "'", dataBaseConnection);
                dataBaseConnection.Open();
                cmd.ExecuteNonQuery();
                dataBaseConnection.Close();
                MessageBox.Show("Уровень доступа аккаунта был изменен.", "Успешное выполнение операции", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DateTime time = DateTime.Today;
                SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + login1 + "', 'Изменил уровень доступа у " + login + " на " + AccLevel + " ', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                dataBaseConnection.Open();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                groupBox2.Visible = false;
            }
            else
                return;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
                AccLevel = 0;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
                AccLevel = 1;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
                AccLevel = 2;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked)
                AccLevel = 3;
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton5.Checked)
                AccLevel = 4;
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton6.Checked)
                AccLevel = 5;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Уверены в том, что хотите удалить учетную запись?", "Подтверждение операции", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                String login = (String)dataGridView2["login", dataGridView2.CurrentCell.RowIndex].Value;
                SqlCommand cmd = new SqlCommand("Delete from Users where userName = '" + login + "'", dataBaseConnection);
                dataBaseConnection.Open();
                cmd.ExecuteNonQuery();
                dataBaseConnection.Close();
                DateTime time = DateTime.Today;
                SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + login1 + "', 'Удалил учетную запись " + login + "', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                dataBaseConnection.Open();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                MessageBox.Show("Удаление выполнено.", "Успешное выполнение операции", MessageBoxButtons.OK, MessageBoxIcon.Information);
                groupBox2.Visible = false;
            }
        }
    }
}
