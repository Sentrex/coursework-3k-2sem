﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Programm
{
    public partial class Tariffs : Form
    {
        SqlConnection dataBaseConnection;
        DataSet ds;
        String login;
        public Tariffs(SqlConnection sql, String lg)
        {
            login = lg;
            dataBaseConnection = sql;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            RegTariff.Visible = false;
            NameTariff.Clear();
            Abonplata.Clear();
        }

        private void Tariffs_Load(object sender, EventArgs e)
        {
            RefreshData();
            ControlBox = false;
        }

        private void RefreshData()
        {
            ds = new DataSet();
            SqlDataAdapter da;
            String search = Search.Text;
            da = new SqlDataAdapter("Select название as 'Название тарифа', Абонплата, код from Тарифы where название like '" + search + "%' order by название", dataBaseConnection);
            da.Fill(ds, "Тарифы");
            dataGridView1.DataSource = ds.Tables["Тарифы"];
            dataGridView1.Columns[2].Visible = false;
            dataGridView1.Update();
        }

        private void Search_TextChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RegTariff.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы подтвержадаете выполнение действия? Его выполнение послечет за собой удаление всех номеров данного тарифа в базе и всей информации о них!", "Подтверждение закрытия тарифа", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                int selectedCode = (int)dataGridView1["код", dataGridView1.CurrentCell.RowIndex].Value;
                String query = "";
                query += "Delete from Пополнение where код_номера in (select код from Номера where код_тарифа = " + selectedCode + ");";
                query += "Delete from Полученные_услуги where код_номера in (select код from Номера where код_тарифа = " + selectedCode + ");";
                query += "Delete from Номера where код_тарифа = " + selectedCode + ";";
                query += "Delete from Тарифы where код = " + selectedCode;
                SqlCommand cmd = new SqlCommand(query, dataBaseConnection);
                dataBaseConnection.Open();
                cmd.ExecuteNonQuery();
                dataBaseConnection.Close();
                DateTime time = DateTime.Today;
                SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + login + "', 'Удалил тариф " + dataGridView1["Название тарифа", dataGridView1.CurrentCell.RowIndex].Value + "', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                dataBaseConnection.Open();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                MessageBox.Show("Тариф успешно закрыт с выполнением очистки базы от упоминаний о номерах, которые находились на нем.", "Успешное выполнение команды", MessageBoxButtons.OK, MessageBoxIcon.Information);
                RefreshData();
            }
            else
                return;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (NameTariff.Text == "")
            {
                MessageBox.Show("Введите название нового тарифа", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (Abonplata.Text == "")
            {
                MessageBox.Show("Введите абонентскую плату для нового тарифа", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            String tarName = NameTariff.Text;      //получение названия тарифа
            Decimal plata = Decimal.Parse(Abonplata.Text.Trim());  //получение абонплаты
            SqlCommand cmd = new SqlCommand("Insert into Тарифы values('" + tarName + "', " + plata + ")");
            cmd.Connection = dataBaseConnection;
            dataBaseConnection.Open();
            cmd.ExecuteNonQuery();
            dataBaseConnection.Close();
            DateTime time = DateTime.Today;
            SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + login + "', 'Открыл тариф " + tarName + "', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
            dataBaseConnection.Open();
            cmd1.ExecuteNonQuery();
            dataBaseConnection.Close();
            RegTariff.Visible = false;
            NameTariff.Clear();
            Abonplata.Clear();
            RefreshData();
            MessageBox.Show("Тариф открыт.", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            int selectedCode = (int)dataGridView1["код", dataGridView1.CurrentCell.RowIndex].Value; //получили код
            if (maskedTextBox1.Text == ""){
                MessageBox.Show("Неверное значение суммы.", "Неверные данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                maskedTextBox1.Focus();
                return;
            }
            else{
                Decimal newsum = Decimal.Parse(maskedTextBox1.Text.Trim());
                SqlCommand cmd = new SqlCommand("Update Тарифы set Абонплата = " + newsum + " where код = " + selectedCode, dataBaseConnection);
                dataBaseConnection.Open();
                cmd.ExecuteNonQuery();
                dataBaseConnection.Close();
                DateTime time = DateTime.Today;
                SqlCommand cmd1 = new SqlCommand("Insert into LogChanges values ('" + login + "', 'Установил новую абонплату тарифу " + dataGridView1["Название тарифа", dataGridView1.CurrentCell.RowIndex].Value + " (" + newsum + ")', '" + DateTime.Now + "', '" + time + "')", dataBaseConnection);
                dataBaseConnection.Open();
                cmd1.ExecuteNonQuery();
                dataBaseConnection.Close();
                MessageBox.Show("Изменение успешно выполнено.", "Успешное выполнение операции", MessageBoxButtons.OK, MessageBoxIcon.Information);
                maskedTextBox1.Clear();
                RefreshData();
            }
        }

    }
}
